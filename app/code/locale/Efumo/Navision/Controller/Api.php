<?php

/**
 * Efumo_Navision
 *
 * @category    Efumo
 * @package     Efumo_Navision
 * @author      Martins Saukums
 * @copyright   Copyright (c) 2017 Efumo, Ltd.(https://www.efumo.lv/)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Efumo_Navision_Controller_Api extends Mage_Core_Controller_Front_Action
{

    const HTTP_CODE_OK = 200;
    const HTTP_CODE_CREATED = 201;
    const HTTP_CODE_BAD_REQUEST = 400;
    const HTTP_CODE_UNAUTHORIZED = 401;
    const HTTP_CODE_NOT_FOUND = 404;
    const HTTP_CODE_INTERNAL_SERVER_ERROR = 500;

    /**
     * @return bool
     */
    public function isAjax()
    {
        return $this->getRequest()->isXmlHttpRequest();
    }

    /**
     * @return bool
     */
    public function isGet()
    {
        return $this->getRequest()->isGet();
    }

    /**
     * @return bool
     */
    public function isPost()
    {
        return $this->getRequest()->isPost();
    }

    /**
     * @return bool
     */
    public function isPut()
    {
        return $this->getRequest()->isPut();
    }

    /**
     * @param $ip
     * @return bool
     */
    protected function _checkIfAllowedByIp($ip)
    {
        return (Mage::helper('core/http')->getRemoteAddr(true) == $ip) ? true : false;
    }

    /**
     * Sets HTTP 200 OK header
     */
    public function setResponseHttpStatusCodeOk()
    {
        $this->setResponseHttpStatusCode(static::HTTP_CODE_OK);
    }

    /**
     * Sets HTTP 201 Created header
     */
    public function setResponseHttpStatusCodeCreated()
    {
        $this->setResponseHttpStatusCode(static::HTTP_CODE_CREATED);
    }

    /**
     * Sets HTTP 400 Bad Request header
     */
    public function setResponseHttpStatusCodeBadRequest()
    {
        $this->setResponseHttpStatusCode(static::HTTP_CODE_BAD_REQUEST);
    }

    /**
     * Sets HTTP 401 Unauthorized header
     */
    public function setResponseHttpStatusCodeUnauthorized()
    {
        $this->setResponseHttpStatusCode(static::HTTP_CODE_UNAUTHORIZED);
    }

    /**
     * Sets HTTP 404 Not Found header
     */
    public function setResponseHttpStatusCodeNotFound()
    {
        $this->setResponseHttpStatusCode(static::HTTP_CODE_NOT_FOUND);
    }

    /**
     * Sets HTTP 500 Internal Server Error header
     */
    public function setResponseHttpStatusCodeIntServerError()
    {
        $this->setResponseHttpStatusCode(static::HTTP_CODE_INTERNAL_SERVER_ERROR);
    }

    /**
     * @param $code
     */
    public function setResponseHttpStatusCode($code)
    {
        $this->getResponse()->setHeader('HTTP/1.1', $code, true);
    }

    /**
     * @param $data
     */
    public function sendJson($data)
    {
        $json = Mage::helper('core')->jsonEncode($data);

        $this->getResponse()->setHeader(Zend_Http_Client::CONTENT_TYPE, 'application/json');
        $this->getResponse()->setBody($json);
    }
}