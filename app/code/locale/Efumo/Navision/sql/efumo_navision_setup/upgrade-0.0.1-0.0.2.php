<?php
/**
 * Efumo_Navision
 *
 * @category    Efumo
 * @package     Efumo_Navision
 * @author      Martins Saukums
 * @copyright   Copyright (c) 2017 Efumo, Ltd.(https://www.efumo.lv/)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

/**
 * @var $installer Mage_Core_Model_Resource_Setup
 */
$installer = $this;
$installer->startSetup();

$priceTable = $installer->getTable('efumo_navision/price');
if ($installer->getConnection()->isTableExists($priceTable)) {
    $installer->getConnection()->dropTable($priceTable);
}

$table = $installer->getConnection()
    ->newTable($priceTable)
    ->addColumn('price_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity' => true,
        'unsigned' => true,
        'nullable' => false,
        'primary'  => true,
    ), 'ID')
    ->addColumn('store_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity' => false,
        'unsigned' => true,
        'nullable' => false,
        'primary'  => false,
    ), 'Store ID')
    ->addColumn('price_key', Varien_Db_Ddl_Table::TYPE_TEXT, 32, array(
        'nullable' => false
    ), 'Price Key')
    ->addColumn('price_includes_vat', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned' => true,
        'nullable' => false,
    ), 'Including VAT')
    ->addColumn('allow_line_disc', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned' => true,
        'nullable' => false,
    ), 'Allow_Line_Disc')
    ->addColumn('minimum_quantity', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned' => true,
        'nullable' => false,
    ), 'Minimum quantity')
    ->addColumn('item_no', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable' => false
    ), 'Item No')
    ->addColumn('sales_type', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable' => false
    ), 'Sales Type')
    ->addColumn('sales_code', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable' => false
    ), 'Sales Code')
    ->addColumn('currency_code', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable' => false
    ), 'Currency Code')
    ->addColumn('unit', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable' => false
    ), 'Unit_of_Measure_Code')
    ->addColumn('variant_code', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable' => false
    ), 'Variant')
    ->addColumn('unit_price', Varien_Db_Ddl_Table::TYPE_FLOAT, null, array(
        'nullable' => false,
    ), 'Price')
    ->addColumn('starting_date', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        'nullable' => false,
        'default'  => Varien_Db_Ddl_Table::TIMESTAMP_INIT
    ), 'Starting at')
    ->addColumn('ending_date', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        'nullable' => false,
    ), 'Ending at');

$table->addIndex(
    $installer->getIdxName(
        'efumo_navision/price',
        ['price_key', 'store_id'],
        Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
    ),
    ['price_key', 'store_id'],
    ['type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE]
);

$installer->getConnection()->createTable($table);

$installer->endSetup();
