<?php
/**
 * Efumo_Navision
 *
 * @category    Efumo
 * @package     Efumo_Navision
 * @author      Martins Saukums
 * @copyright   Copyright (c) 2017 Efumo, Ltd.(https://www.efumo.lv/)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

/**
 * @var $installer Mage_Core_Model_Resource_Setup
 */
$installer = $this;
$installer->startSetup();

$syncTable = $installer->getTable('efumo_navision/sync');
if ($installer->getConnection()->isTableExists($syncTable)) {
    $installer->getConnection()->dropTable($syncTable);
}

$table = $installer->getConnection()
    ->newTable($syncTable)
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity' => true,
        'unsigned' => true,
        'nullable' => false,
        'primary'  => true,
    ), 'ID')
    ->addColumn('type', Varien_Db_Ddl_Table::TYPE_TEXT, 32, array(
        'nullable' => false
    ), 'Sync Type')
    ->addColumn('item_id', Varien_Db_Ddl_Table::TYPE_TEXT, 82, array(
        'unsigned' => true,
        'nullable' => false,
    ), 'Type ID')
    ->addColumn('retrieve', Varien_Db_Ddl_Table::TYPE_BOOLEAN, null, array(
        'unsigned' => true,
        'nullable' => true,
    ), 'Import from Nav')
    ->addColumn('send', Varien_Db_Ddl_Table::TYPE_BOOLEAN, null, array(
        'unsigned' => true,
        'nullable' => true,
    ), 'Send to Nav');

$installer->getConnection()->createTable($table);

$installer->endSetup();
