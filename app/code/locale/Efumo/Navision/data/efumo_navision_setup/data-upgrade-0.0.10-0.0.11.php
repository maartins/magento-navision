<?php
/**
 * Efumo_Navision
 *
 * @category    Efumo
 * @package     Efumo_Navision
 * @author      Mairis Saukums
 * @copyright   Copyright (c) 2017 Efumo, Ltd.(https://www.efumo.lv/)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

$installer = $this;
$installer->startSetup();

//Remove navision attribute set
try {
    Mage::getModel('eav/entity_attribute_set')
        ->load('Navision', 'attribute_set_name')->delete();

} catch (Exception $e) {
    Mage::logException($e->getMessage());
}

$installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'hafele_no', array(
    'group' => 'Navision',
    'attribute_set' => 'Default',
    'label' => 'Hafele No.',
    'type' => 'varchar',
    'input' => 'text',
    'visible' => true,
    'required' => false,
    'position' => 21,
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'default' => 0,
    'searchable' => true,
    'used_in_product_listing' => true,
));

$installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'web_id', array(
    'group' => 'Navision',
    'attribute_set' => 'Default',
    'label' => 'Web ID',
    'type' => 'varchar',
    'input' => 'text',
    'visible' => true,
    'is_visible_on_front' => 1,
    'required' => false,
    'position' => 22,
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'default' => 0,
    'searchable' => true,
    'used_in_product_listing' => true,
));


$installer->endSetup();