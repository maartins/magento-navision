<?php
/**
 * Efumo_Navision
 *
 * @category    Efumo
 * @package     Efumo_Navision
 * @author      Martins Saukums
 * @copyright   Copyright (c) 2017 Efumo, Ltd.(https://www.efumo.lv/)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

/** @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

$installer->addAttribute('customer', 'credit_limit', array(
    'type'     => 'decimal',
    'label'    => 'Credit Limit',
    'input'    => 'text',
    'visible'  => true,
    'required' => true,
));

$installer->addAttribute('customer', 'credit_spent', array(
    'type'     => 'decimal',
    'label'    => 'Total Credit Spent',
    'input'    => 'text',
    'visible'  => true,
    'required' => true,
));

$installer->endSetup();
