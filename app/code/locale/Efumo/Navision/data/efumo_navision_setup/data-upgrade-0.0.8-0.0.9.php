<?php
/**
 * Efumo_Navision
 *
 * @category    Efumo
 * @package     Efumo_Navision
 * @author      Martins Saukums
 * @copyright   Copyright (c) 2017 Efumo, Ltd.(https://www.efumo.lv/)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

/** @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

$installer->addAttribute('customer', 'is_company_admin', array(
    'type'     => 'int',
    'label'    => 'Is the customer an admin of the company',
    'input'    => 'select',
    'source'   => 'eav/entity_attribute_source_boolean',
    'visible'  => true,
    'required' => false,
));

$installer->endSetup();
