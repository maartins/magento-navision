<?php
/**
 * Efumo_Navision
 *
 * @category    Efumo
 * @package     Efumo_Navision
 * @author      Mairis Saukums
 * @copyright   Copyright (c) 2017 Efumo, Ltd.(https://www.efumo.lv/)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

/** @var $installer Mage_Core_Model_Resource_Setup */

$installer = $this;
$installer->startSetup();

$installer->removeAttribute('catalog_product', 'manufacturer');


$installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'manufacturer', array(
    'group'                   => 'Navision',
    'attribute_set'           => 'Default',
    'label'                   => 'Manufacturer',
    'type'                    => 'varchar',
    'input'                   => 'text',
    'visible'                 => true,
    'required'                => false,
    'position'                => 23,
    'global'                  => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'default'                 => 0,
    'searchable'              => true,
    'used_in_product_listing' => true,
));


$installer->endSetup();