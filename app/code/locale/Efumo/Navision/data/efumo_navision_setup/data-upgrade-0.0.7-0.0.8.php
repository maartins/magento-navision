<?php
/**
 * Efumo_Navision
 *
 * @category    Efumo
 * @package     Efumo_Navision
 * @author      Martins Saukums
 * @copyright   Copyright (c) 2017 Efumo, Ltd.(https://www.efumo.lv/)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

/** @var $installer Mage_Eav_Model_Entity_Setup */
$installer = $this;
$installer->startSetup();

$installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'hafele_no', array(
    'group'                   => 'Navision',
    'attribute_set'           => 'Hafele',
    'label'                   => 'Hafele No.',
    'type'                    => 'varchar',
    'input'                   => 'text',
    'visible'                 => true,
    'required'                => false,
    'position'                => 21,
    'global'                  => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'default'                 => 0,
    'searchable'              => true,
    'used_in_product_listing' => true,
));

$installer->endSetup();
