<?php
/**
 * Efumo_Navision
 *
 * @category    Efumo
 * @package     Efumo_Navision
 * @author      Martins Saukums
 * @copyright   Copyright (c) 2017 Efumo, Ltd.(https://www.efumo.lv/)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

/** @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

$entityTypeId = Mage::getModel('catalog/product')
    ->getResource()
    ->getEntityType()
    ->getId();

$attributeSet = Mage::getModel('eav/entity_attribute_set')
    ->setEntityTypeId($entityTypeId)
    ->setAttributeSetName('Hafele');

$attributeSet->validate();
$attributeSet->save();
$attributeSet->initFromSkeleton(4)->save();

$installer->endSetup();
