<?php

/**
 * Efumo_Navision
 *
 * @category    Efumo
 * @package     Efumo_Navision
 * @author      Martins Saukums
 * @copyright   Copyright (c) 2017 Efumo, Ltd.(https://www.efumo.lv/)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Efumo_Navision_Navision_SpecialpriceController extends Efumo_Navision_Controller_Api
{

    public function preDispatch()
    {
        parent::preDispatch();

        //  TODO :: check for ip address

    }

    public function updateAction()
    {
        try {

            Mage::helper('efumo_navision')->setSpecialPriceQueue();

            $timecreated = strftime("%Y-%m-%d %H:%M:%S", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
            $timescheduled = strftime("%Y-%m-%d %H:%M:%S", mktime(date("H"), date("i") + 1, date("s"), date("m"), date("d"), date("Y")));
            $jobCode = 'efumo_navision_import_price';

            $schedule = Mage::getModel('cron/schedule');
            $schedule->setJobCode($jobCode)
                ->setCreatedAt($timecreated)
                ->setScheduledAt($timescheduled)
                ->setStatus(Mage_Cron_Model_Schedule::STATUS_PENDING)
                ->save();

            $this->sendJson('Price saved');
            $this->setResponseHttpStatusCodeOk();

        } catch (Exception $e) {
            Mage::throwException($e->getMessage());
            $this->setResponseHttpStatusCodeIntServerError();
        }
    }
}