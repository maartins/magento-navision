<?php

/**
 * Efumo_Navision
 *
 * @category    Efumo
 * @package     Efumo_Navision
 * @author      Martins Saukums
 * @copyright   Copyright (c) 2017 Efumo, Ltd.(https://www.efumo.lv/)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Efumo_Navision_Navision_ProductController extends Efumo_Navision_Controller_Api
{

    public function preDispatch()
    {
        parent::preDispatch();

        //  TODO :: check for ip address

    }

    public function updateAction()
    {
        $web_id = (int)$this->getRequest()->getParam('web_id');
        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

        try {

            if ($web_id) {
                Mage::helper('efumo_navision')->setProductInQueue($web_id);

                $timecreated = strftime("%Y-%m-%d %H:%M:%S", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
                $timescheduled = strftime("%Y-%m-%d %H:%M:%S", mktime(date("H"), date("i") + 1, date("s"), date("m"), date("d"), date("Y")));
                $jobCode = 'efumo_navision_import_products';

                $schedule = Mage::getModel('cron/schedule');
                $schedule->setJobCode($jobCode)
                    ->setCreatedAt($timecreated)
                    ->setScheduledAt($timescheduled)
                    ->setStatus(Mage_Cron_Model_Schedule::STATUS_PENDING)
                    ->save();

                $this->sendJson('Product saved');
                $this->setResponseHttpStatusCodeOk();
            } else {
                $this->sendJson('Please send web_id');
                $this->setResponseHttpStatusCodeIntServerError();
            }

        } catch (Exception $e) {
            Mage::throwException($e->getMessage());
            $this->setResponseHttpStatusCodeIntServerError();
        }
    }

    public function deleteAction()
    {
        $web_id = $this->getRequest()->getPost('web_id');

        try {
            Mage::register('isSecureArea', true);
            Mage::helper('efumo_navision')->deleteProductByWebId($web_id);
            Mage::unregister('isSecureArea');
        } catch (Exception $e) {
            Mage::throwException($e->getMessage());
            $this->setResponseHttpStatusCodeIntServerError();
        }

    }
}