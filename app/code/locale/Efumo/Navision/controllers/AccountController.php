<?php
/**
 * Efumo_Navision
 *
 * @category    Efumo
 * @package     Efumo_Navision
 * @author      Martins Saukums
 * @copyright   Copyright (c) 2017 Efumo, Ltd.(https://www.efumo.lv/)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

require_once 'Mage/Customer/controllers/AccountController.php';

class Efumo_Navision_AccountController extends Mage_Customer_AccountController
{
    /**
     * Join information from Navision
     *
     * @return void
     */
    public function createPostAction()
    {
        $this->addInformationFromNavision();

        return parent::createPostAction();
    }

    /**
     * Adds company info to request
     *
     * @return void
     */
    protected function addInformationFromNavision()
    {
        $request = $this->getRequest();
        $registrationNumber = preg_replace('~[^\d]~', '', $request->getParam('taxvat'));

        if ($registrationNumber) {
            $registrationNumber = 'LV' . $registrationNumber;


            $companyInfo = Mage::helper('efumo_navision')->fetchCompanyInfo(
                $registrationNumber,
                $request->getParam('email'),
                $request->getParam('firstname'),
                $request->getParam('lastname')
            );

            $request->setPost('country_id', $companyInfo->Country);
            $request->setPost('company', $companyInfo->CompanyName);
            $request->setPost('street', array($companyInfo->Address, $companyInfo->Address2));
            $request->setPost('city', $companyInfo->City);
            $request->setPost('taxvat', $registrationNumber);
            $request->getPost('postcode', $companyInfo->Post_Code);

            $this->getRequest()->setPost('navision_contact_number', $companyInfo->ContactNo);

        }
    }
}