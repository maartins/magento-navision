<?php

/**
 * Efumo_Navision
 *
 * @category    Efumo
 * @package     Efumo_Navision
 * @author      Martins Saukums
 * @copyright   Copyright (c) 2017 Efumo, Ltd.(https://www.efumo.lv/)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Efumo_Navision_Adminhtml_CategoryimportController extends Mage_Adminhtml_Controller_Action
{

    public function importAction()
    {
        try {
            Mage::app('admin');
            Mage::getModel('efumo_navision/import_categories')->importAndAssignProducts();
            Mage::app()->getResponse()->setBody('done');

        } catch (Exception $e) {
            Mage::logException($e);
        }
    }
}