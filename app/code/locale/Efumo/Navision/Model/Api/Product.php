<?php

/**
 * Efumo_Navision
 *
 * @category    Efumo
 * @package     Efumo_Navision
 * @author      Martins Saukums
 * @copyright   Copyright (c) 2017 Efumo, Ltd.(https://www.efumo.lv/)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Efumo_Navision_Model_Api_Product extends Mage_Api_Model_Resource_Abstract
{
    /**
     * Renames product with the given sku to the given name
     *
     * @param $navisionId
     * @param $name
     * @return bool
     */
    public function rename($navisionId, $name)
    {
        $product = Mage::getModel('catalog/product')->load($navisionId, 'sku');

        if (!$product || !$product->getId()) {
            return false;
        }

        $product->setName($name)->save();

        return true;
    }

    /**
     * Update product data
     *
     * @param       $navisionId
     * @param array $data
     * @return bool
     */
    public function modify($navisionId, array $data)
    {
        $product = Mage::getModel('catalog/product')->load($navisionId, 'sku');

        if (!$product || !$product->getId()) {
            return false;
        }

        return true;
    }

    /**
     * Create new product
     *
     * @param       $navisionId
     * @param array $data
     * @return bool
     */
    public function insert($navisionId, array $data)
    {
        $product = Mage::getModel('catalog/product')->load($navisionId, 'sku');

        if ($product && $product->getId()) {
            return false;
        }

        return true;
    }

    /**
     * Delete product with the given $navisionId
     *
     * @param $navisionId
     * @return bool
     */
    public function delete($navisionId)
    {
        $product = Mage::getModel('catalog/product')->load($navisionId, 'sku');

        if (!$product || !$product->getId()) {
            return false;
        }

        $product->delete();

        return true;
    }
}