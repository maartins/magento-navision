<?php

/**
 * Efumo_Navision
 *
 * @category    Efumo
 * @package     Efumo_Navision
 * @author      Martins Saukums
 * @copyright   Copyright (c) 2017 Efumo, Ltd.(https://www.efumo.lv/)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Efumo_Navision_Model_Observer
{
    /**
     * @var array
     */
    protected $prices = [];

    /**
     * Set custom price
     *
     * @param $observer
     * @return void
     */
    public function addPriceToProduct($observer)
    {
        $product = $observer->getProduct();
        $this->preloadPricesForProducts([$product->getSku()]);
        $this->setPrice($product);
    }

    /**
     * Sets custom price to product collection
     *
     * @param $observer
     * @return void
     */
    public function addPriceToProductCollection($observer)
    {
        /** @var Mage_Catalog_Model_Resource_Product_Collection $collection */
        $collection = $observer->getCollection();
        $collection->setFlag('tier_price_added', true);
        $this->preloadPricesForProducts($collection->getColumnValues('sku'));

        foreach ($collection as $product) {
            $this->setPrice($product);
        }
    }

    /**
     *
     * Fix price when add to cart
     * @param $observer
     */
    public function addPriceToProductCart($observer)
    {
        $event = $observer->getEvent();
        $product = $event->getQuoteItem();

        $this->preloadPricesForProducts([$product->getSku()]);

        if (!isset($this->prices[$product->getSku()])) {
            return;
        }

        $prices = $this->prices[$product->getSku()];

        foreach ($prices as $price) {
            if (!$this->isPriceApplicable($price)) {
                continue;
            }

            $calculatedPrice = $this->calculatePrice($price);

            if ($price->getMinimumQuantity() > 1) {
                $tierPrices[] = [
                    'cust_group' => 32000,
                    'price_qty' => $price->getMinimumQuantity(),
                    'price' => $calculatedPrice,
                    'website_price' => $calculatedPrice,
                    'website_id' => 0
                ];
                continue;
            }

            $normalPrices[] = $calculatedPrice;
        }

        $lowestNormalPrice = min($normalPrices);

        $quote_item = $event->getQuoteItem();
        $quote_item->setOriginalCustomPrice($lowestNormalPrice);
        $quote_item->getQuote()->save();
    }

    /**
     * Sets custom prices on product
     *
     * @param $product
     * @return void
     */
    protected function setPrice(Mage_Catalog_Model_Product $product)
    {
        if (!isset($this->prices[$product->getSku()])) {
            return;
            $this->preloadPricesForProducts();
            $this->setPrice($product);
        }

        $prices = $this->prices[$product->getSku()];
        $tierPrices = [];
        $normalPrices = [$product->getFinalPrice()];

        foreach ($prices as $price) {
            if (!$this->isPriceApplicable($price)) {
                continue;
            }

            $calculatedPrice = $this->calculatePrice($price);

            if ($price->getMinimumQuantity() > 1) {
                $tierPrices[] = [
                    'cust_group' => 32000,
                    'price_qty' => $price->getMinimumQuantity(),
                    'price' => $calculatedPrice,
                    'website_price' => $calculatedPrice,
                    'website_id' => 0
                ];
                continue;
            }

            $normalPrices[] = $calculatedPrice;
        }

        $lowestNormalPrice = min($normalPrices);
        $product->setFinalPrice($lowestNormalPrice);

        $product->setData('tier_price', $tierPrices);
    }

    /**
     * Preloads prices for given products
     *
     * @param $skus
     * @return void
     */
    protected function preloadPricesForProducts($skus)
    {
        $skus = array_diff($skus, array_keys($this->prices));

        if (!$skus) {
            return;
        }

        $prices = Mage::getModel('efumo_navision/price')
            ->getCollection()
            ->addFieldToFilter('item_no', ['in' => $skus])
            ->addFieldToFilter('store_id', Mage::app()->getStore()->getId());

        foreach ($prices as $price) {
            if (!isset($this->prices[$price->item_no])) {
                $this->prices[$price->item_no] = [];
            }

            $this->prices[$price->item_no][] = $price;
        }
    }

    /**
     * Calculates price correctly
     *
     * @param $price
     * @return mixed
     */
    protected function calculatePrice($price)
    {
        return $price->getUnitPrice();
    }

    /**
     * Checks whether current customer can use the given price
     *
     * @param $price
     * @return void
     */
    protected function isPriceApplicable($price)
    {
        $customer = Mage::getSingleton('customer/session')->getCustomer();

        // checks customer code
        if (
            $price->getSalesType() == 'Customer' &&
            $price->getSalesCode() != $customer->getNavisionContactNumber() &&
            $price->getSalesCode() != $customer->getNavisionClientNumber()
        ) {
            return false;
        }

        return true;
    }

    /**
     * @param Varien_Event_Observer $observer
     * @return mixed
     */
    public function addPriceToCollection(Varien_Event_Observer $observer)
    {
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        $collection = $observer->getCollection();


        if ($navNumber = ($customer->getNavisionClientNumber()) ? $customer->getNavisionClientNumber() : $customer->getNavisionContactNumber()) {

            $price = [];
            $new = $collection->joinTable(
                array('price' => 'efumo_navision/price'), 'item_no=sku',
                array('company_final_price' => 'unit_price', 'sales_code' => 'sales_code'))
                ->addFilterToMap('sales_code', 'sales_code')
                ->addFieldToFilter('sales_code', $navNumber);

            foreach ($new->getData() as $data) {
                $price[$data['entity_id']] = $data['company_final_price'];
            }

            foreach ($collection as $_item) {
                if (array_key_exists($_item->getId(), $price)) {
                    $_item->setFinalPrice($price[$_item->getId()]);
                }
            }
        }

        return $collection;
    }

    /**
     *
     */
    public function syncCreditMemo(Varien_Event_Observer $observer)
    {
        $event = $observer->getEvent();
        $customer = $event->getCustumer();

        Mage::getModel('efumo_navision/import_customers_creditmemo')->getCreditMemoForCompany($customer->getData('navision_client_number'));
    }

    public function syncOrders(Varien_Event_Observer $observer)
    {
        $event = $observer->getEvent();
        $customer = $event->getCustumer();

        Mage::getSingleton('efumo_navision/import_orders')->importWithCustomerNumber($customer->getData('navision_client_number'));
    }
}