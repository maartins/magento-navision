<?php

/**
 * Efumo_Navision
 *
 * @category    Efumo
 * @package     Efumo_Navision
 * @author      Martins Saukums
 * @copyright   Copyright (c) 2017 Efumo, Ltd.(https://www.efumo.lv/)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Efumo_Navision_Model_Import_Orders_Items extends Efumo_Navision_Model_Import_Products_Info
{
    const WEB_SERVICE = 'SalesOrderSalesLines';
    const ITEM_NO_FIELD = 'Document_No';

    /**
     * Returns filter
     *
     * @param $batch
     * @return array
     */
    protected function getFilter($batch)
    {
        $orderIds = implode(' or ', $batch);
        return ['$filter' => "Document_Type eq 'ORDER' and ($orderIds)"];
    }
}