<?php

/**
 * Efumo_Navision
 *
 * @category    Efumo
 * @package     Efumo_Navision
 * @author      Martins Saukums
 * @copyright   Copyright (c) 2016 Efumo, Ltd.(https://www.efumo.lv/)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Efumo_Navision_Model_Import_Companies extends Efumo_Navision_Model_Import_Abstract
{
    const LAST_MODIFIED_KEY = 'navision_last_customer_import';

    const GROUP_COMPANY_ID = 4;
    const GROUP_PRIVATE_ID = 1;

    /**
     * @var Efumo_Navision_Model_Import_Customers_Clients
     */
    protected $clients;

    /**
     * Runs import
     *
     * @return void
     */
    public function import()
    {
        $this->clients = Mage::getSingleton('efumo_navision/import_customers_clients');


        // we only need contact cards, because they contain the user information

        $this->processAllPages('ContactCard', ['$filter' => 'No eq \'CT069639\''], function ($collection) {
            if (isset($collection->value)) {
                $this->clients->loadInfo($collection->value);
                $this->processCompanies($collection->value);
            }
        });

        // save last modified date
    }

    /**
     * Processes clients page
     *
     * @param $clients
     * @return void
     */
    protected function processCompanies($companies)
    {
        $importData = [];

        /** @var AvS_FastSimpleImport_Model_Import $import */
        $import = Mage::getModel('fastsimpleimport/import');
        foreach ($companies as $company) {
            if ($built = $this->buildCompany($company)) {
                $importData[] = $built;
            }
        }
        if ($importData) {
            $import->setAllowRenameFiles(false)
                ->setPartialIndexing(true)
                ->processCustomerImport($importData, 'replace');
        }
    }

    /**
     * Builds customer info object
     *
     * @param $customer
     * @return array
     */
    protected function buildCompany($company)
    {
        $client = $this->clients->getInfo($company);
        if (!$company->E_Mail) {
            $company->E_Mail = $company->No . '@example.com';
        }

        list($firstName, $lastName) = explode(' ', trim($company->Name) . ' ', 2);
        $lastName = trim($lastName);

        if (!$client || !$client->No || !filter_var($company->E_Mail, FILTER_VALIDATE_EMAIL)) {
            return;
        }

        return [
            'email' => $company->E_Mail,
            '_website' => 'base',
            'group_id' => $this->getGroupId($company),
            'firstname' => $firstName,
            'lastname' => $lastName ?: '-',
            'company' => $company->Company_Name ?: $company->Name,
            '_address_company' => $company->Company_Name ?: $company->Name,
            '_address_firstname' => $firstName,
            '_address_lastname' => $lastName ?: '-',
            '_address_street' => $company->Address . "\n" . $company->Address_2,
            '_address_postcode' => $company->Post_Code ?: 'LV-',
            '_address_city' => $company->City ?: '-',
            '_address_country_id' => $company->Country_Region_Code ?: 'LV',
            '_address_telephone' => $company->Mobile_Phone_No ?: $company->Phone_No,
            '_address_default_billing_' => 1,
            '_address_default_shipping_' => 1,
            'navision_contact_number' => $company->No,
            'navision_client_number' => $client->No,
            'credit_limit' => $client->Credit_Limit_LCY,
            'credit_spent' => $client->Credit_Limit_LCY - $client->AvailableCreditLCY,
            'account_type' => $this->getAccountType($company),
            'reward_update_notification' => 0,
            'reward_warning_notification' => 0
        ];
    }

    /**
     * Fetch client number for the given customer
     *
     * @param Mage_Customer_Model_Customer $customer
     * @return bool|string
     */
    public function getClientNumber(Mage_Customer_Model_Customer $customer)
    {
        if ($contactNumber = $customer->getNavisionContactNumber()) {
            $client = $this->api->find('BusinessRelationContacts', [
                'Business_Relation_Code' => 'KLNT',
                'Contact_No' => $contactNumber
            ]);

            if ($client) {
                return $client->No;
            }
        }

        return false;
    }

    /**
     * Fetch group id
     *
     * @param $customer
     * @return int
     */
    protected function getGroupId($customer)
    {
        if ($customer->Type == 'Company') {
            return static::GROUP_COMPANY_ID;
        }

        return static::GROUP_PRIVATE_ID;
    }

    /**
     * Fetch account type
     *
     * @param $customer
     * @return string
     */
    protected function getAccountType($customer)
    {
        if ($customer->Type == 'Company') {
            return Magebit_Register_Helper_Data::TYPE_COMPANY;
        }

        return Magebit_Register_Helper_Data::TYPE_PRIVATE;
    }
}