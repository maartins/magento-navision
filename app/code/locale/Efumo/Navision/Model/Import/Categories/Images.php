<?php

/**
 * Efumo_Navision
 *
 * @category    Efumo
 * @package     Efumo_Navision
 * @author      Martins Saukums
 * @copyright   Copyright (c) 2017 Efumo, Ltd.(https://www.efumo.lv/)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Efumo_Navision_Model_Import_Categories_Images
{

    /**
     * @param $category
     * @return null|string
     */
    public function getImage($category)
    {

        $attachmentPath = $this->getAttachmentPath();
        $fileName = md5($category->ETag) . '.' . $category->File_Extension;

        $attachmentFullPath = $attachmentPath . $fileName;


        if (file_exists($category->File_Name) && $this->isImage($category)) {
            copy($category->File_Name, $attachmentFullPath);
            return $fileName;
        } else {
            return null;
        }
    }

    /**
     * @return string
     */
    protected function getAttachmentPath()
    {
        return Mage::getBaseDir('media') . '/import/';
    }

    /**
     * Check if attachment is an image
     *
     * @param $attachment
     * @return bool
     */
    protected function isImage($attachment)
    {
        $imageExtensions = ['jpg', 'gif', 'png', 'jpeg'];

        return in_array(strtolower($attachment->File_Extension), $imageExtensions);
    }
}