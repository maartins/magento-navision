<?php

/**
 * Efumo_Navision
 *
 * @category    Efumo
 * @package     Efumo_Navision
 * @author      Martins Saukums
 * @copyright   Copyright (c) 2017 Efumo, Ltd.(https://www.efumo.lv/)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Efumo_Navision_Model_Import_Hafele extends Efumo_Navision_Model_Import_Abstract
{
    /**
     * An associative array of all skus -> productIds
     *
     * @var array
     */
    protected $existingSkus;

    /**
     * Runs import
     *
     * @return void
     */
    public function import()
    {
        $stores = array('lv_lat');//, 'ee_est');
        foreach ($stores as $store) {
            $this->initStore($store);
            $this->importHafeleItems();
        }
    }

    /**
     * Imports products
     *
     * @return void
     */
    protected function importHafeleItems()
    {
        // read all pages and execute product import after each page has been read
        $this->processAllPages('HafeleItems', [], function ($collection) {
            if (isset($collection->value)) {
                $this->processHafeleItems($collection->value);
            }
        });
    }

    /**
     * Processes a batch of products (up to 1000)
     *
     * @param array $items
     * @return void
     */
    protected function processHafeleItems(array $items)
    {
        $importData = [];
        foreach ($items as $item) {
            $importData[] = $this->buildIteme($item);
        }


        if ($importData) {

            $totalCount = count($importData);

            $batchSize = 1000;

            for ($idx = 0; $idx * $batchSize < $totalCount; $idx++) {
                $items = array_slice($importData, $idx * $batchSize, $batchSize);
                Mage::getResourceModel('efumo_hafele/item')->saveToTable($items);
            }
        }
    }

    /**
     *
     * @param $item
     * @return array
     */
    protected function buildIteme($item)
    {
        return [
            'item_no' => $item->Item_No,
            'hafele_no' => $item->Customs_No,
            'price' => $item->Price,
            'vendor_country' => $item->Vendor_Country_Code,
            'title' => $item->Title,
            'unit' => $item->Unit_of_Measure,
            'min_order_qty' => $item->Min_Order_Quantity,
            'sales_price' => $item->Sales_Price,
            'net_weight' => $item->Net_Weight
        ];
    }
}