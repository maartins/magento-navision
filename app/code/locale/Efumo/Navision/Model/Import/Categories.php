<?php

/**
 * Efumo_Navision
 *
 * @category    Efumo
 * @package     Efumo_Navision
 * @author      Martins Saukums
 * @copyright   Copyright (c) 2017 Efumo, Ltd.(https://www.efumo.lv/)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Efumo_Navision_Model_Import_Categories extends Efumo_Navision_Model_Import_Abstract
{
    /**
     * An associative array of all categories
     *
     * @var array
     */
    protected $existingCategories;

    /**
     * Language to store mapping
     *
     * @var array
     */
    protected $languageToStore;

    /**
     * st
     */
    protected $_categories;

    /**
     * Stores exsting products
     * @var
     */
    protected $_existingCategories;

    /**
     * @var
     */
    protected $_categorySingleton;

    /**
     * Stor existing products
     * @var
     */
    protected $_productSku;

    const Latvian = 'Name_in_Latvian';
    const Estonian = 'Name_in_Russian';
    const English = 'Name_in_English';
    const Russian = 'Name_in_Russian';

    /**
     * Efumo_Navision_Model_Import_Categories constructor.
     */
    public function __construct()
    {
        $this->_categorySingleton = Mage::getSingleton('catalog/category');
        parent::__construct();
    }

    /**
     * Runs import
     *
     * @return void
     */
    public function import()
    {
        $this->loadExistingCategories();
        $this->importCategories();
        $this->translate();
        $this->createAnchor();
    }

    /**
     * Runs import and assign products to category
     */
    public function importAndAssignProducts()
    {
        $this->import();
        $this->connectProductsWithCategories();
    }

    /**
     * Initiates a new store
     *
     * @param null $store
     * @return $this
     */
    public function initStore($store = null)
    {
        parent::initStore($store);

        $this->languageToStore = [];

        return $this;
    }

    /**
     * Imports categories
     *
     * @return void
     */
    protected function importCategories()
    {
        $categories = [];
        $this->processAllPages('WebCategory', [], function ($collection) use (&$categories) {
            if (isset($collection->value)) {
                $categories = array_merge($categories, $collection->value);
            }
        });
        // we need to process all categories
        $this->processCategories($categories);
    }

    /**
     *  Load all existing products
     *
     * @return mixed
     */
    public function getExistingProducts()
    {
        if (!isset($this->_productSku)) {
            $collection = Mage::getModel('catalog/product')->getCollection()
                ->addAttributeToSelect('entity_id')
                ->addAttributeToSelect('web_id');

            foreach ($collection as $product) {
                $this->_productSku[$product->getId()] = (int)$product->getWebId();
            }
        }

        return $this->_productSku;

    }

    /**
     * Load all existing categories
     *
     * @return mixed
     */
    public function getExistingCategories()
    {

        if (!isset($this->_existingCategories)) {
            $collection = Mage::getModel('catalog/category')->getCollection()
                ->addAttributeToSelect('id')
                ->addAttributeToSelect('navision_code');

            foreach ($collection as $category) {
                $this->_existingCategories[$category->getData('navision_code')] = $category->getId();
            }
        }

        return $this->_existingCategories;

    }

    /**
     * Assign product to category
     */
    public function connectProductsWithCategories()
    {
        $_categoryRelations = Mage::getModel('efumo_navision/import_products_categories')->getCategories();
        $products = $this->getExistingProducts();
        $categories = $this->getExistingCategories();
        $table = Mage::getSingleton('core/resource')->getTableName('catalog/category_product');
        $conn = Mage::getModel('core/resource')->getConnection('core_write');

        try {
            $conn->truncateTable($table);
            foreach ($products as $id => $webId) {
                if (array_key_exists($webId, $_categoryRelations)) {

                    foreach ($_categoryRelations[$webId] as $cat) {
                        if (array_key_exists($cat, $categories)) {
                            $productOrderData = array(
                                'product_id' => (int)$id,
                                'category_id' => (int)$categories[$cat]
                            );
                            $conn->insert($table, $productOrderData);
                        }
                    }
                }
            }

            $indexProcess = Mage::getSingleton('index/indexer')->getProcessByCode('catalog_category_product');
            if ($indexProcess) {
                $indexProcess->reindexAll();
            }

        } catch (Exception $e) {
            Mage::logException($e);
        }
    }

    /**
     * Processes a batch of categories (up to 1000)
     *
     * @param array $categories
     * @return void
     */
    protected function processCategories(array $categories)
    {
        /** @var AvS_FastSimpleImport_Model_Import $import */
        $import = Mage::getModel('fastsimpleimport/import');

        $categories = $this->filterGoodCategories($categories);

        $this->_categories = $categories;

        $categories = $this->sortCategoriesByLevel($categories);


        $importData = [];
        foreach ($categories as $category) {
            $importData = array_merge($importData, $this->buildCategory($category));
        }

        if ($importData) {
            $import->processCategoryImport($importData);

        }
    }

    /**
     * Creates third level as anchor for filters
     */
    public function createAnchor()
    {
        $categories = Mage::getModel('catalog/category')
            ->getCollection()
            ->addAttributeToSelect('is_anchor')
            ->addAttributeToFilter('entity_id', array("gt" => 3))
            ->setOrder('entity_id');

        foreach ($categories as $category) {
            echo $category->getId() . "\t" . $category->getName() . "\n";
            $category->setIsAnchor(1);
            $category->save();
        }
    }

    /**
     * Builds category info array
     *
     * @param $category
     * @return array
     */
    protected function buildCategory($category)
    {
        $categories = [];

        // build base product
        $baseCategory = [
            '_root' => 'Default Category',
            '_category' => $category->Path,
            'is_active' => Mage::helper('efumo_navision')->__('Yes'),
            'include_in_menu' => Mage::helper('efumo_navision')->__('Yes'),
            'description' => $category->Name_in_Latvian,
            'available_sort_by' => 'position',
            'default_sort_by' => 'position',
            'navision_code' => $category->Code,
            'thumbnail' => ($category->File_Name) ? Mage::getSingleton('efumo_navision/import_categories_images')->getImage($category) : null
        ];

        $categories[] = $baseCategory;

        return $categories;
    }

    /**
     * Loads existing skus
     *
     * @return void
     */
    protected function loadExistingCategories()
    {
        $idsToNames = [];
        $categories = Mage::getModel('catalog/category')
            ->getCollection()
            ->addAttributeToSelect('name')
            ->addFieldToFilter('level', array('gteq' => 2));

        foreach ($categories as $category) {
            $idsToNames[$category->getId()] = str_replace('/', '\\/', $category->getName());
        }

        foreach ($categories as $category) {
            $parts = array_slice(explode('/', $category->getPath()), 2);
            $parts = array_map(function ($id) use ($idsToNames) {
                return $idsToNames[$id];
            }, $parts);
            $this->existingCategories[implode('/', $parts)] = $category;
        }
    }

    /**
     * Filters out bad categories and returns only the good ones
     *
     * @param array $categories
     * @return array
     */
    protected function filterGoodCategories(array $categories)
    {
        // filter out categories without code and name
        $goodCategories = [];
        foreach ($categories as $category) {
            if ($category->Code && $category->Name_in_Latvian) {
                $goodCategories[$category->Code] = $category;
            }
        }

        // drop any category that has invalid parent
        do {
            $countBefore = count($goodCategories);
            $goodCategories = array_filter($goodCategories,
                function ($category) use ($goodCategories) {
                    return !$category->Parent_Web_Category_Code || isset($goodCategories[$category->Code]);
                }
            );
        } while (count($goodCategories) < $countBefore);

        return $goodCategories;
    }

    /**
     * Sorts the given categories by their level asc
     *
     * @param array $categories
     * @return array
     */
    protected function sortCategoriesByLevel(array $categories)
    {
        foreach ($categories as &$category) {

            $path = [str_replace('/', '\\/', $category->Name_in_Latvian)];
            $parent = $category;

            while ($parent->Parent_Web_Category_Code) {
                $parent = $categories[$parent->Parent_Web_Category_Code];
                $path[] = str_replace('/', '\\/', $parent->Name_in_Latvian);
            }
            $category->Level = count($path);
            $category->Path = implode('/', array_reverse($path));

        }
        // sort by it
        usort($categories, function ($a, $b) {
            return strnatcasecmp($a->Level, $b->Level);
        });

        return $categories;
    }

    /**
     * Import translate for cat
     */
    public function translate()
    {

        $translateArray = [];

        $this->processAllPages('WebCategory', [], function ($collection) use (&$translateArray) {
            if (isset($collection->value)) {

                foreach ($collection->value as $cat) {
                    $translateArray[$cat->Code]['Latvian'] = $cat->{self::Latvian};
                    $translateArray[$cat->Code]['Estonian'] = $cat->{self::Estonian};
                    $translateArray[$cat->Code]['English'] = $cat->{self::English};
                    $translateArray[$cat->Code]['Russian'] = $cat->{self::Russian};
                }
            }
        });

        if (!count($this->_existingCategories)) {
            $this->getExistingCategories();
        }

        foreach ($this->_existingCategories as $k => $v) {
            if (array_key_exists($k, $translateArray)) {
                foreach ($translateArray[$k] as $key => $val) {
                    if (Mage::getModel('core/store')->load($key, 'name')->getId() != 0) {
                        $this->_categorySingleton->setId($v);
                        $this->_categorySingleton->setName($val);
                        $this->_categorySingleton->setStoreId(Mage::getModel('core/store')->load($key, 'name')->getId());
                        Mage::getModel('catalog/category')->getResource()->saveAttribute($this->_categorySingleton, 'name');
                    }
                }
            }
        }
    }
}