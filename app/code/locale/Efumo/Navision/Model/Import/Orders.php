<?php

/**
 * Efumo_Navision
 *
 * @category    Efumo
 * @package     Efumo_Navision
 * @author      Martins Saukums
 * @copyright   Copyright (c) 2016 Efumo, Ltd.(https://www.efumo.lv/)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Efumo_Navision_Model_Import_Orders extends Efumo_Navision_Model_Import_Abstract
{
    /**
     * @var Efumo_Navision_Model_Import_Orders_Items
     */
    protected $items;

    public function __construct()
    {
        $this->items = Mage::getSingleton('efumo_navision/import_orders_items');

        parent::__construct();
    }

    public function import()
    {
        // TODO: Implement import() method.
    }

    public function importWithCustomerNumber($customer)
    {
        $this->getOrders($customer);
    }

    protected function getOrders($customer)
    {
        $this->processAllPages('SalesOrder', ['$filter' => "Bill_to_Customer_No eq '$customer'"], function ($collection) {
            if (isset($collection->value)) {
                $this->processOrders($collection->value);
            }
        });
    }

    /**
     * Processes a batch of orders (up to 1000)
     *
     * @param array $orders
     * @return void
     */
    protected function processOrders(array $orders)
    {
        foreach ($orders as $order) {
            $this->processOrder($order);
        }
    }

    /**
     * Process a single order...
     *
     * @param $order
     * @return void
     */
    protected function processOrder($order)
    {
        $model = $this->getOrderInstance($order);

        // the order is already completed
        if ($model->getId() || $model->getStatus() == Mage_Sales_Model_Order::STATE_COMPLETE) {
            return;
        }

        // do nothing with an other that we already have for now..
        if (!$model->getId()) {
            $this->addPaymentInfo($model, $order);
            $this->addBillingAddress($model, $order);
            $this->addShippingAddress($model, $order);

            $this->addCustomerInfo($model, $order);

            if (!$model->getCustomerId()) {
                return;
            }

            $this->addItems($model, $order);
        }
        $this->addOrderInformation($model, $order);
        $model->save();

    }

    /**
     * Fetches an order or creates a new instance of it...
     *
     * @param $order
     * @return Mage_Sales_Model_Order
     */
    protected function getOrderInstance($order)
    {
        if ($order->External_Document_No) {
            $model = Mage::getModel('sales/order')->loadByIncrementId($order->External_Document_No);
        }
        if (!isset($model) || !$model->getId()) {
            $model = Mage::getModel('sales/order')->loadByIncrementId($order->No);
        }
        if (!$model->getIncrementId()) {
            $model->setIncrementId($order->No);
        }

        return $model;
    }

    /**
     * Adds payment info...
     *
     * @param Mage_Sales_Model_Order $model
     * @param                        $order
     * @return void
     */
    protected function addPaymentInfo(Mage_Sales_Model_Order $model, $order)
    {
        $payment = Mage::getModel('sales/order_payment')
            ->setMethod('free');

        $model->setPayment($payment);
    }

    /**
     * Adds billing address...
     *
     * @param Mage_Sales_Model_Order $model
     * @param                        $order
     * @return void
     */
    protected function addBillingAddress(Mage_Sales_Model_Order $model, $order)
    {
        $address = Mage::getModel('sales/order_address')
            //->setCountryId($order->Ship_to_Country_Region_Code ?: 'LV')
            ->setCity($order->Bill_to_City)
            ->setCompany($order->Bill_to_Name)
            ->setFirstname($order->Bill_to_Name)
            ->setPostcode($order->Bill_to_Post_Code)
            ->setStreetFull([$order->Bill_to_Address, $order->Bill_to_Address_2]);

        $model->setBillingAddress($address);
    }

    /**
     * Adds shipping address...
     *
     * @param Mage_Sales_Model_Order $model
     * @param                        $order
     * @return void
     */
    protected function addShippingAddress(Mage_Sales_Model_Order $model, $order)
    {
        $address = Mage::getModel('sales/order_address')
            // ->setCountryId($order->Ship_to_Country_Region_Code ?: 'LV')
            ->setCity($order->Ship_to_City)
            ->setCompany($order->Ship_to_Name)
            ->setFirstname($order->Ship_to_Name)
            ->setPostcode($order->Ship_to_Post_Code)
            ->setStreetFull([$order->Ship_to_Address, $order->Ship_to_Address_2]);

        $model->setShippingAddress($address);
    }

    /**
     * Adds basic order information...
     *
     * @param Mage_Sales_Model_Order $model
     * @param                        $order
     * @return void
     */
    protected function addOrderInformation(Mage_Sales_Model_Order $model, $order)
    {
        if (!$model->getState()) {
            $model->setState(Mage_Sales_Model_Order::STATE_NEW, true);
            $model->save();
            $this->completeOrder($model);
        }
    }

    /**
     * Automatically invoices and ships an order...
     *
     * @param Mage_Sales_Model_Order $order
     * @return void
     * @throws Exception
     */
    protected function completeOrder(Mage_Sales_Model_Order $order)
    {
        if ($order->getState() == Mage_Sales_Model_Order::STATE_NEW || $order->getState() == Mage_Sales_Model_Order::STATE_PROCESSING) {
            if (!$order->canInvoice()) {
                return;
            }

            //START Handle Invoice
            $invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice();
            $invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::CAPTURE_OFFLINE);
            $invoice->register();

            $invoice->getOrder()->setCustomerNoteNotify(false);
            $invoice->getOrder()->setIsInProcess(true);

            $invoice->save();

            //START Handle Shipment
            $shipment = $order->prepareShipment();
            $shipment->register();

            $order->setIsInProcess(true);
            $shipment->save();
        }
    }

    /**
     * Adds customer info...
     *
     * @param Mage_Sales_Model_Order $model
     * @param                        $order
     * @return void
     */
    protected function addCustomerInfo(Mage_Sales_Model_Order $model, $order)
    {
        /** @var Mage_Customer_Model_Customer $customer */
        $customer = Mage::getModel('customer/customer')
            ->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('navision_client_number', $order->Bill_to_Customer_No)
            ->setPageSize(1)
            ->getFirstItem();

        if (!$customer || !$customer->getId()) {
            return;
        }

        $model->setCustomerId($customer->getId())
            ->setCustomerDob($customer->getDob())
            ->setCustomerEmail($customer->getEmail())
            ->setCustomerFirstname($customer->getFirstname())
            ->setCustomerLastname($customer->getLastname())
            ->setCustomerGroupId($customer->getGroupId())
            ->setCustomerIsGuest(false);
    }

    /**
     * Adds items...
     *
     * @param Mage_Sales_Model_Order $model
     * @param                        $order
     * @return void
     */
    protected function addItems(Mage_Sales_Model_Order $model, $order)
    {
        $this->items->loadInfo([$order]);
        $items = $this->items->getInfo($order);
        $discount = 0;
        foreach ($items as $item) {
            $discount += $item->Line_Discount_Amount;
            $this->addItem($model, $item);
        }

        if (isset($item)) {
            $model->setTaxAmount($item->Amount_Including_VAT - $item->Amount);
//            $model->setDiscountAmount($discount);
            $model->setSubtotal($item->Amount);
//            $model->setSubtotalInclTax($item->Total_Amount_Excl_VAT);
            $model->setGrandTotal($item->Amount_Including_VAT - $discount);
//
            $model->setBaseTaxAmount($item->Amount_Including_VAT - $item->Amount);
//            $model->setBaseDiscountAmount($discount);
            $model->setBaseSubtotal($item->Amount);
            $model->setBaseSubtotalInclTax($item->Amount_Including_VAT);
            $model->setBaseGrandTotal($item->Amount_Including_VAT - $discount);
        }
    }

    /**
     * Adds item to the order
     *
     * @param Mage_Sales_Model_Order $order
     * @param                        $item
     * @return void
     */
    protected function addItem(Mage_Sales_Model_Order $order, $item)
    {
        /** @var Mage_Sales_Model_Order_Item $model */
        $model = $order->getItemsCollection()->getNewEmptyItem();

        foreach ($order->getItemsCollection() as $itemModel) {
            if (!$itemModel->getUsed() && $itemModel->getSku() == $item->No) {
                $model = $itemModel;
                break;
            }
        }

        // product does not exist yet
        $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $item->No);
        $model->setSku($item->No)
            ->setUsed(true)
            ->setQtyOrdered($item->Quantity)
//            ->setQtyShipped($item->Quantity_Shipped)
//            ->setQtyInvoiced($item->Quantity_Invoiced)
            ->setDiscountPercent($item->Line_Discount_Percent)
            ->setDiscountAmount($item->Line_Discount_Amount)
            ->setBaseDiscountAmount($item->Line_Discount_Amount)
            ->setPrice($item->Unit_Price)
            ->setPriceInclTax($item->Amount_Including_VAT)
            ->setBasePrice($item->Unit_Price)
            ->setBasePriceInclTax($item->Amount_Including_VAT)
            ->setRowTotal($item->Amount_Including_VAT)
            ->setTaxAmount(($item->Amount_Including_VAT / $item->Line_Amount) - 1)
            ->setBaseTaxAmount(($item->Amount_Including_VAT / $item->Line_Amount) - 1)
            ->setTaxPercent(round((($item->Amount_Including_VAT / $item->Line_Amount) - 1) * 100))
//            ->setIsQtyDecimal($item->Quantity != floor($item->Quantity))
//            ->setName($item->Description)
            ->setOriginalPrice($item->Unit_Price)
            ->setBaseOriginalPrice($item->Unit_Price)
            ->setProductId($product ? $product->getId() : null);

        $order->addItem($model);
    }
}
