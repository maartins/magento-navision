<?php

/**
 * Efumo_Navision
 *
 * @category    Efumo
 * @package     Efumo_Navision
 * @author      Martins Saukums
 * @copyright   Copyright (c) 2017 Efumo, Ltd.(https://www.efumo.lv/)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Efumo_Navision_Model_Import_Customers_Customercard extends Efumo_Navision_Model_Import_Products_Info
{
    const WEB_SERVICE = 'CustomerCard';
    const ITEM_NO_FIELD = 'No';

    /**
     * Returns an array of product data
     *
     * @param array $collection
     * @return array
     */
    protected function processInfo(array $collection)
    {
        foreach ($collection as $value) {
            $itemNo = $value->{static::ITEM_NO_FIELD};
            $this->data[$itemNo] = $value;
        }
    }

    /**
     * Fetch given models data
     *
     * @param $model
     * @return mixed
     */
    public function getInfo($model)
    {
        if (!isset($this->data[$model->{static::NO_FIELD}])) {
            return;
        }

        return $this->data[$model->{static::NO_FIELD}];
    }
}