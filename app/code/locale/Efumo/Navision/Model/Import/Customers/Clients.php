<?php

/**
 * Efumo_Navision
 *
 * @category    Efumo
 * @package     Efumo_Navision
 * @author      Martins Saukums
 * @copyright   Copyright (c) 2016 Efumo, Ltd.(https://www.efumo.lv/)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Efumo_Navision_Model_Import_Customers_Clients extends Efumo_Navision_Model_Import_Products_Info
{
    const WEB_SERVICE = 'BusinessRelationContacts';
    const ITEM_NO_FIELD = 'Contact_No';
    const NO_FIELD = 'Company_No';


    /**
     * @var Efumo_Navision_Model_Import_Customers_Customercards
     */
    protected $customerCards;

    public function __construct()
    {
        $this->customerCards = Mage::getSingleton('efumo_navision/import_customers_customercard');

        parent::__construct();
    }

    /**
     * Initializes import instance for the given store
     *
     * @param null|string|int $store
     * @return $this
     */
    public function initStore($store = null)
    {
        parent::initStore($store);

        $this->customerCards->initStore($store);

        return $this;
    }

    /**
     * Returns an array of product data
     *
     * @param array $collection
     * @return array
     */
    protected function processInfo(array $collection)
    {
        foreach ($collection as $value) {
            $itemNo = $value->{static::ITEM_NO_FIELD};
            $this->data[$itemNo] = $value;
        }

        $this->customerCards->loadInfo($collection);
    }

    /**
     * Fetch given models data
     *
     * @param $model
     * @return mixed
     */
    public function getInfo($model)
    {

        if (!isset($this->data[$model->{static::NO_FIELD}])) {
            return;
        }

        return $this->customerCards->getInfo($this->data[$model->{static::NO_FIELD}]);
    }


    /**
     * Returns filter
     *
     * @param $batch
     * @return array
     */
    protected function getFilter($batch)
    {
        return ['$filter' => implode(' or ', $batch) . ' and Business_Relation_Code eq \'KLNT\''];
    }
}