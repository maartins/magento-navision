<?php

/**
 * Efumo_Navision
 *
 * @category    Efumo
 * @package     Efumo_Navision
 * @author      Martins Saukums
 * @copyright   Copyright (c) 2017 Efumo, Ltd.(https://www.efumo.lv/)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Efumo_Navision_Model_Import_Customers_Creditmemo extends Efumo_Navision_Model_Import_Abstract
{
    /**
     * Credit memo import
     */
    public function import()
    {
        // TODO: Implement import() method.
    }

    /**
     * @param $companyNumber
     */
    public function getCreditMemoForCompany($companyNumber)
    {
        $params['$filter'] = "No eq '$companyNumber'";
        $this->processAllPages('CustomerCard', $params, function ($collection) use ($companyNumber) {
            if (isset($collection->value)) {
                foreach ($collection->value as $val) {
                    $this->setCreditMemoForAllCompanyUsers($val, $companyNumber);
                }
            }
        });
    }

    /**
     * @param $company
     * @param $companyNumber
     */
    protected function setCreditMemoForAllCompanyUsers($company, $companyNumber)
    {
        if (is_object($company)) {
            $customers = Mage::getModel('customer/customer')->getCollection()
                ->addAttributeToFilter('navision_client_number', ['eq' => $companyNumber]);

            foreach ($customers as $customer) {
                Mage::getModel('customer/customer')->load($customer->getId())->setCreditLimit($company->Credit_Limit_LCY)->setCreditSpent($company->Credit_Limit_LCY - $company->AvailableCreditLCY)->save();
            }
        }
    }
}