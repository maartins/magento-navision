<?php

/**
 * Efumo_Navision
 *
 * @category    Efumo
 * @package     Efumo_Navision
 * @author      Martins Saukums
 * @copyright   Copyright (c) 2017 Efumo, Ltd.(https://www.efumo.lv/)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Efumo_Navision_Model_Import_Salesprices extends Efumo_Navision_Model_Import_Abstract
{
    /**
     * Runs import
     *
     * @return void
     */
    public function import()
    {
        $stores = array('lv_lat');
        foreach ($stores as $store) {
            $this->initStore($store);
            $this->importPrices();
        }
    }

    /**
     * Imports sales prices
     *
     * @return void
     */
    protected function importPrices()
    {
        // read all pages and execute product import after each page has been read
        $this->processAllPages('SalesPrice', [], function ($collection) {
            $this->processPrices($collection->value);
        });
    }

    /**
     * Processes a batch of prices (up to 1000)
     *
     * @param array $prices
     * @return void
     */
    protected function processPrices(array $prices)
    {
        $priceKeys = [];
        $adapter = Mage::getSingleton('core/resource')->getConnection('core_write');
        foreach ($prices as $price) {
            $adapter->insertOnDuplicate(Mage::getSingleton('core/resource')->getTableName('efumo_navision/price'), [
                'price_key' => $priceKeys[] = $this->generatePriceKey($price),
                'store_id' => $this->store->getId(),
                'price_includes_vat' => $price->Price_Includes_VAT,
                'allow_line_disc' => $price->Allow_Line_Disc,
                'minimum_quantity' => $price->Minimum_Quantity,
                'item_no' => $price->Item_No,
                'sales_type' => $price->Sales_Type,
                'sales_code' => $price->Sales_Code,
                'currency_code' => $price->Currency_Code,
                'unit' => $price->Unit_of_Measure_Code,
                'unit_price' => $price->Unit_Price,
                'starting_date' => (new DateTime($price->Starting_Date))->format('Y-m-d H:i:s'),
                'ending_date' => (new DateTime($price->Ending_Date))->format('Y-m-d H:i:s'),
                'variant_code' => $price->Variant_Code,
            ]);
        }
    }

    /**
     * Generates price key for items
     *
     * @param $price
     * @return string
     */
    protected function generatePriceKey($price)
    {
        $primaryKeyFields = [
            'Item_No',
            'Sales_Code',
            'Currency_Code',
            'Starting_Date',
            'Sales_Type',
            'Minimum_Quantity',
            'Unit_of_Measure_Code',
            'Variant_Code'
        ];

        $values = array();
        foreach ($primaryKeyFields as $field) {
            $values[] = $price->{$field};
        }

        return md5(json_encode($values));
    }
}