<?php

/**
 * Efumo_Navision
 *
 * @category    Efumo
 * @package     Efumo_Navision
 * @author      Martins Saukums
 * @copyright   Copyright (c) 2017 Efumo, Ltd.(https://www.efumo.lv/)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
abstract class Efumo_Navision_Model_Import_Abstract
{
    /**
     * API instance
     *
     * @var Efumo_Navision_Model_Navision_Odata
     */
    protected $api;

    /**
     * Current store
     *
     * @var Mage_Core_Model_Store
     */
    protected $store;

    /**
     * Initiate FastSimpleImport
     */
    public function __construct()
    {
        $this->initStore();
    }

    /**
     * Initializes import instance for the given store
     *
     * @param null|string|int $store
     * @return $this
     */
    public function initStore($store = null)
    {
        $this->store = Mage::app()->getStore($store);
        $this->api = Mage::getModel('efumo_navision/navision_odata', $this->store->getId());

        return $this;
    }

    /**
     * Runs import
     *
     * @return void
     */
    abstract public function import();

    /**
     * Imports products
     *
     * @param          $pageUrl
     * @param          $params
     * @param callable $callback
     */
    protected function processAllPages($pageUrl, $params, Callable $callback)
    {
        do {
            $collection = $this->api->request('GET', $pageUrl, $params);
            $callback($collection);
            $pageUrl = $this->getNextPageUrl($collection);
        } while ($pageUrl);
    }

    /**
     * Returns URL for next page
     *
     * @param $collection
     * @return bool
     */
    protected function getNextPageUrl($collection)
    {
        if (!isset($collection->{'@odata.nextLink'})) {
            return false;
        }
        return $collection->{'@odata.nextLink'};
    }
}