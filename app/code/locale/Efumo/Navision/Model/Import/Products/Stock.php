<?php

/**
 * Efumo_Navision
 *
 * @category    Efumo
 * @package     Efumo_Navision
 * @author      Martins Saukums
 * @copyright   Copyright (c) 2017 Efumo, Ltd.(https://www.efumo.lv/)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Efumo_Navision_Model_Import_Products_Stock extends Efumo_Navision_Model_Import_Products_Info
{
    const WEB_SERVICE = 'ItemAvailableQty';

    /**
     * SOAP Api
     *
     * @var Efumo_Navision_Model_Navision_Soap
     */
    protected $soap;

    /**
     * Initializes import instance for the given store
     *
     * @param null|string|int $store
     * @return $this
     */
    public function initStore($store = null)
    {
        parent::initStore($store);

        $this->soap = Mage::getModel('efumo_navision/navision_soap');

        return $this;
    }

    /**
     *
     * @param $product
     * @return array
     */
    public function getStockForProduct($product)
    {
        $images = [];
        $stocks = $this->getInfo($product);
        foreach ($stocks as $stock) {
            //  var_dump($this->getStock($stock));
        }

        //return $images;
    }

    /**
     * Fetch products image
     *
     * @param $attachment
     * @return null|string
     */
    protected function getStock($stock)
    {
        $response = $this->soap->request('ItemAvailableQty', [
            'iD' => $stock->Item_No
        ]);

        return;
    }
}