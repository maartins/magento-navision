<?php

/**
 * Efumo_Navision
 *
 * @category    Efumo
 * @package     Efumo_Navision
 * @author      Mārtiņš Saukums
 * @copyright   Copyright (c) 2017 Efumo, Ltd.(https://www.efumo.lv/)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Efumo_Navision_Model_Import_Products_Attachments extends Efumo_Navision_Model_Import_Products_Info
{
    const WEB_SERVICE = 'ItemAttachments';
    const ITEM_NO_FIELD = 'Item_No';

    /**
     * Adds images from attachments
     *
     * @param $product
     * @return array
     */
    public function getImagesForProduct($product)
    {
        $images = [];

        if (array_key_exists($product->No, $this->data)) {
            foreach ($this->data[$product->No] as $attachment) {

                if ($this->isImage($attachment) && $img = $this->getAttachment($attachment,$product)) {
                    $images[$attachment->Attachement_No] = $img;
                }
            }
        }

        return  (count($images)) ? $images : null;
    }

    /**
     * Fetch products image
     *
     * @param $attachment
     * @return null|string
     */
    protected function getAttachment($attachment,$product)
    {
        $attachmentPath = $this->getAttachmentPath();
        $fileName = md5($attachment->ETag) . '.' . $attachment->File_Extension;

        // for better os support when browsing attachments
        $fileName = substr($fileName, 0, 2) . '/' . substr($fileName, 2, 2) . '/' . $fileName;
        $attachmentFullPath = $attachmentPath . $fileName;

        if (!is_dir(dirname($attachmentFullPath))) {
            mkdir(dirname($attachmentFullPath), 0777, true);

        }

        if (file_exists($attachment->File_Name)) {
            copy($attachment->File_Name,$attachmentFullPath);

            return $fileName;
        } else {
            return null;
        }
    }

    /**
     * @return string
     */
    protected function getAttachmentPath()
    {
        return Mage::getBaseDir('media') . '/import/';
    }

    /**
     * Check if attachment is an image
     *
     * @param $attachment
     * @return bool
     */
    protected function isImage($attachment)
    {
        $imageExtensions = ['jpg', 'gif', 'png', 'jpeg'];

        return in_array(strtolower($attachment->File_Extension), $imageExtensions);
    }
}