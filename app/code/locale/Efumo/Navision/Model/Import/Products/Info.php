<?php

/**
 * Efumo_Navision
 *
 * @category    Efumo
 * @package     Efumo_Navision
 * @author      Martins Saukums
 * @copyright   Copyright (c) 2016 Efumo, Ltd.(https://www.efumo.lv/)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
abstract class Efumo_Navision_Model_Import_Products_Info extends Efumo_Navision_Model_Import_Abstract
{
    const RESET_BEFORE_LOAD = true;
    const NO_FORMAT = "'%s'";
    const ITEM_NO_FIELD = 'Item_No';
    const NO_FIELD = 'No';
    const BATCH_SIZE = 100;
    const WEB_SERVICE = '';
    const USE_BATCHES = false; // about 2x faster when using batches

    /**
     * Array of data
     *
     * @var array
     */
    protected $data = [];

    /**
     * Runs import
     *
     * @return void
     */
    public function import()
    {
        // do nothing
    }

    /**
     * Loads data
     *
     * @param array $collection
     * @return void
     */
    public function loadInfo(array $collection)
    {
        $batches = $this->getBatches($collection);

        if (static::RESET_BEFORE_LOAD) {
            $this->data = [];
        }

        if (static::USE_BATCHES) {
            $batchFilters = [];
            foreach ($batches as $batch) {
                $batchFilters[] = $this->getFilter($batch);
            }
            $collection = $this->api->batch(static::WEB_SERVICE, $batchFilters);
            $this->processInfo($collection);
        } else {
            foreach ($batches as $batch) {
                $filters = $this->getFilter($batch);
                $this->processAllPages(static::WEB_SERVICE, $filters, function ($collection) {
                    if (isset($collection->value) && is_array($collection->value)) {
                        $this->processInfo($collection->value);
                    }
                });
            }
        }
    }

    /**
     * Fetch given models data
     *
     * @param $model
     * @return array
     */
    public function getInfo($model)
    {
        if (!isset($this->data[$model->{static::NO_FIELD}])) {
            return [];
        }

        return $this->data[$model->{static::NO_FIELD}];
    }

    /**
     * Returns an array of product data
     *
     * @param array $collection
     * @return array
     */
    protected function processInfo(array $collection)
    {
        foreach ($collection as $value) {
            $itemNo = $value->{static::ITEM_NO_FIELD};
            if (!isset($this->data[$itemNo])) {
                $this->data[$itemNo] = [];
            }

            $this->data[$itemNo][] = $value;

        }
    }

    /**
     * Returns batches of product skus
     *
     * @param array $items
     * @return array
     */
    protected function getBatches(array $items)
    {
        $itemFilters = [];
        foreach ($items as $item) {
            if ($this->getInfo($item)) {
                continue;
            }

            $itemFilters[] = static::ITEM_NO_FIELD . " eq " . sprintf(static::NO_FORMAT, $item->{static::NO_FIELD});
        }

        $itemFilters = array_unique($itemFilters);

        $batches = array_chunk($itemFilters, static::BATCH_SIZE);

        return $batches;
    }

    /**
     * Returns filter
     *
     * @param $batch
     * @return array
     */
    protected function getFilter($batch)
    {
        return ['$filter' => implode(' or ', $batch)];
    }
}