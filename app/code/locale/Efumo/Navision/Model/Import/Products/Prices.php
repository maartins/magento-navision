<?php

/**
 * Efumo_Navision
 *
 * @category    Efumo
 * @package     Efumo_Navision
 * @author      Martins Saukums
 * @copyright   Copyright (c) 2017 Efumo, Ltd.(https://www.efumo.lv/)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Efumo_Navision_Model_Import_Products_Prices extends Efumo_Navision_Model_Import_Products_Info
{
    const WEB_SERVICE = 'SalesPrice';

    /**
     * Returns filter
     *
     * @param $batch
     * @return array
     */
    protected function getFilter($batch)
    {
        return ['$filter' => "Sales_Type eq 'All Customers' and (" . implode(' or ', $batch) . ')'];
    }

    /**
     * Fetch given product's prices
     *
     * @param $product
     * @return array
     */
    public function getInfo($product)
    {
        if (!isset($this->data[$product->No])) {
            return 0;
        }

        $price = $this->data[$product->No][0];
        return $price->Unit_Price;
    }
}