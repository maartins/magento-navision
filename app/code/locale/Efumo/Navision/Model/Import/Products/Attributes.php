<?php

/**
 * Efumo_Navision
 *
 * @category    Efumo
 * @package     Efumo_Navision
 * @author      Martins Saukums
 * @copyright   Copyright (c) 2016 Efumo, Ltd.(https://www.efumo.lv/)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Efumo_Navision_Model_Import_Products_Attributes extends Efumo_Navision_Model_Import_Products_Info
{
    const ITEM_NO_FIELD = 'No';
    const WEB_SERVICE = 'ItemAttributeValueMapping';

    /**
     * @var Efumo_Navision_Model_Import_Products_Attributes_AttributeSet
     */
    protected $attributeSet;

    /**
     * @var Efumo_Navision_Model_Import_Products_Attributes_Attributes
     */
    protected $attributes;

    /**
     * var Efumo_Navision_Model_Import_Products_Attributes_Values
     */
    protected $values;

    /**
     * var Efumo_Navision_Model_Import_Products_Attributes_ValuesTranslate
     */
    protected $valuesTranslate;

    /**
     * @var Efumo_Navision_Model_Import_Products_Attributes_AttributesTranslate
     */
    protected $attributeTranslate;

    /**
     * @var Mage_Eav_Model_Entity_Attribute[]
     */
    protected $loadedAttributes = array();

    /**
     * @var array
     */
    protected $loadedAttributeOptions = array();

    /**
     * Efumo_Navision_Model_Import_Products_Attributes constructor.
     */
    public function __construct()
    {
        $this->attributeSet = Mage::getSingleton('efumo_navision/import_products_attributes_attributeSet');
        $this->values = Mage::getSingleton('efumo_navision/import_products_attributes_values');
        $this->valuesTranslate = Mage::getSingleton('efumo_navision/import_products_attributes_valuesTranslate');
        $this->attributeTranslate = Mage::getSingleton('efumo_navision/import_products_attributes_attributesTranslate');
        $this->attributes = Mage::getSingleton('efumo_navision/import_products_attributes_attributes');
        parent::__construct();
    }

    /**
     * Initializes import instance for the given store
     *
     * @param null|string|int $store
     * @return $this
     */
    public function initStore($store = null)
    {
        parent::initStore($store);

        $this->values->initStore($store);
        $this->valuesTranslate->initStore($store);
        $this->attributeTranslate->initStore($store);
        $this->attributes->initStore($store);

        return $this;
    }

    /**
     *
     * @param $product
     * @return array
     */
    public function getAttributesForProduct($product)
    {
        $attributes = [];

        foreach ($this->getInfo($product) as $attributeId => $valueId) {
            $attributeLabel = $this->attributes->getInfo($attributeId);
            $attributeTranslate = $this->attributeTranslate->getInfo($attributeId);
            $valueTranslateLabel = $this->valuesTranslate->getInfo($valueId);
            $valueLabel = $this->values->getInfo($valueId);
            $attribute = $this->getAttributeInstance($attributeLabel, $attributeTranslate);
            $this->_addAttributeValue($attribute->getId(), $valueLabel, $valueTranslateLabel);
            $attributes[str_replace('-', '_', $attribute->getAttributeCode())] = $valueLabel;
        }

        return $attributes;

    }

    /**
     * @param $attributeLabel
     * @param $attributeTranslate
     * @param $attributeCode
     * @param int $values
     * @param int $productTypes
     * @param int $setInfo
     * @return bool
     */
    public function createAttribute($attributeLabel, $attributeTranslate, $attributeCode, $values = -1, $productTypes = -1, $setInfo = -1)
    {
        $labelText = trim($attributeLabel);
        $attributeCode = str_replace('-', '_', trim($attributeCode));

        if ($labelText == '' || $attributeCode == '') {
            return false;
        }

        if ($values === -1)
            $values = array();

        if ($productTypes === -1)
            $productTypes = array();

        if ($setInfo !== -1 && (isset($setInfo['SetID']) == false || isset($setInfo['GroupID']) == false)) {
            echo "Please provide both the set-ID and the group-ID of the attribute-set if you'd like to subscribe to one." . "<br/>";
            return false;
        }

        /**
         * If option, then will be for configurable, else placeholder and value from attributeValue
         */
        $data = array(
            'is_global' => '1', //For configurable
            'frontend_input' => 'select', //For configurable
            'default_value_text' => '',
            'default_value_yesno' => '0',
            'default_value_date' => '',
            'default_value_textarea' => '',
            'is_unique' => '0',
            'is_required' => '0',
            'frontend_class' => '',
            'is_searchable' => '1',
            'is_visible_in_advanced_search' => '1',
            'is_comparable' => '1',
            'is_used_for_promo_rules' => '0',
            'is_html_allowed_on_front' => '1',
            'is_visible_on_front' => '0',
            'used_in_product_listing' => '0',
            'used_for_sort_by' => '0',
            'is_configurable' => '1',
            'is_filterable' => '2',
            'is_filterable_in_search' => '1',
            'backend_type' => 'int',
            'default_value' => '',
            'is_user_defined' => '0',
            'is_visible' => '1',
            'is_used_for_price_rules' => '0',
            'position' => '0',
            'is_wysiwyg_enabled' => '0',
            'backend_model' => '',
            'attribute_model' => '',
            'backend_table' => '',
            'frontend_model' => '',
            'source_model' => 'eav/entity_attribute_source_table', //For configurable
            'note' => '',
            'frontend_input_renderer' => '',
        );

        // Now, overlay the incoming values on to the defaults.
        foreach ($values as $key => $newValue)
            if (isset($data[$key]) == false) {
                echo "Attribute feature [$key] is not valid." . "<br/>";
                return false;
            } else
                $data[$key] = $newValue;

        // Valid product types: simple, grouped, configurable, virtual, bundle, downloadable, giftcard
        $data['apply_to'] = $productTypes;
        $data['attribute_code'] = str_replace('-', '_', trim($attributeCode));
        $data['frontend_label'] = array(
            0 => $labelText,
            1 => '',
            3 => '',
            2 => '',
            4 => '',
        );

        $model = Mage::getModel('catalog/resource_eav_attribute');

        $model->addData($data);

        if ($setInfo !== -1) {
            $model->setAttributeSetId($setInfo['SetID']);
            $model->setAttributeGroupId($setInfo['GroupID']);
        }

        $entityModel = Mage::getModel('eav/entity')->setType('catalog_product');
        $entityTypeID = $entityModel->getTypeId();

        $model->setEntityTypeId($entityTypeID);
        $model->setIsUserDefined(1);


        try {
            $model->save();
        } catch (Exception $ex) {
            echo "Attribute [$labelText] could not be saved: " . $ex->getMessage() . "<br/>";
            return false;
        }

        $attribute = Mage::getModel('eav/entity_attribute')->load($model->getId());

        $labelPerSW = [];
        //Default value
        $labelPerSW[0] = $attribute->getFrontendLabel();
        foreach (Mage::app()->getStores() as $store) {
            $language = defined("Efumo_Navision_Model_Import_Products_Attributes_AttributesTranslate::" . $store->getName()) ? constant("Efumo_Navision_Model_Import_Products_Attributes_AttributesTranslate::" . $store->getName()) : null;
            if (array_key_exists($language, $attributeTranslate)) {
                $labelPerSW[$store->getId()] = $attributeTranslate[$language];
            }
        }

        $attribute->setFrontendLabel($labelPerSW)->save();

        $this->loadedAttributes[$attributeCode] = $attribute;
    }

    /**
     * Creates option for the given attribute code
     *
     * @param  $arg_attribute
     * @param                                 $arg_value
     * @return void
     * @throws Exception
     */
    protected function _addAttributeValue($arg_attribute, $arg_value, $valueTranslateLable)
    {
        $attribute_model = Mage::getModel('eav/entity_attribute');
        $attribute = $attribute_model->load($arg_attribute);

        if (!$this->_attributeValueExists($arg_attribute, $arg_value)) {
            $value['option'] = array($arg_value, $arg_value);
            $result = array('value' => $value);
            $attribute->setData('option', $result);
            $attribute->save();

            $labelPerSW = [];
            //Default value
            $labelPerSW[0] = $arg_value;
            foreach (Mage::app()->getStores() as $store) {
                $language = defined("Efumo_Navision_Model_Import_Products_Attributes_ValuesTranslate::" . $store->getName()) ? constant("Efumo_Navision_Model_Import_Products_Attributes_ValuesTranslate::" . $store->getName()) : null;
                $labelPerSW[$store->getId()] = $valueTranslateLable[$language];
            }

            $data['option']['value'] = array($attribute->getSource()->getOptionId($arg_value) => $labelPerSW);
            $attribute_model->addData($data);
            $attribute->save();

        }

        $attribute_options_model = Mage::getModel('eav/entity_attribute_source_table');
        $attribute_options_model->setAttribute($attribute);
        $options = $attribute_options_model->getAllOptions(false);

        foreach ($options as $option) {
            if ($option['label'] == $arg_value) {
                return $option['value'];
            }
        }
        return false;
    }

    /**
     * Check if attribute option exists
     *
     * @param $arg_attribute
     * @param $arg_value
     * @return bool
     */
    protected function _attributeValueExists($arg_attribute, $arg_value)
    {
        $attribute_model = Mage::getModel('eav/entity_attribute');
        $attribute_options_model = Mage::getModel('eav/entity_attribute_source_table');
        $attribute = $attribute_model->load($arg_attribute);

        $attribute_options_model->setAttribute($attribute);
        $options = $attribute_options_model->getAllOptions(false);

        foreach ($options as $option) {
            if ($option['label'] == $arg_value) {
                return $option['value'];
            }
        }

        return false;
    }


    /**
     * Returns an array of product data
     *
     * @param array $collection
     * @return array
     */
    protected function processInfo(array $collection)
    {
        foreach ($collection as $value) {
            $itemNo = $value->{self::ITEM_NO_FIELD};
            if (!isset($this->data[$itemNo])) {
                $this->data[$itemNo] = [];
            }

            $this->data[$itemNo][$value->Item_Attribute_ID] = $value->Item_Attribute_Value_ID;
        }

        $this->attributes->loadInfo($collection);
        $this->values->loadInfo($collection);
        $this->valuesTranslate->loadInfo($collection);
        $this->attributeTranslate->loadInfo($collection);
    }

    /**
     * Fetch attribute code
     *
     * @param $label
     * @return string
     */
    public function getAttributeCode($label)
    {
        return Mage::getSingleton('catalog/product_url')->formatUrlKey($label);
    }

    /**
     * Fetch attribute instance
     *
     * @param $attributeLabel
     * @param $attributeTranslate
     * @return Mage_Eav_Model_Entity_Attribute
     */
    protected function getAttributeInstance($attributeLabel, $attributeTranslate)
    {
        if (!$this->loadedAttributes) {
            $this->loadAttributes();
        }
        //Convert to url key
        $code = $this->getAttributeCode($attributeLabel);

        $code = str_replace('-', '_', trim($code));
        // create attribute if it doesn't exist
        if (!isset($this->loadedAttributes[$code])) {
            $this->createAttribute($attributeLabel, $attributeTranslate, $code, $values = -1, $productTypes = -1, $setInfo = -1);
            $this->attributeSet->addAttributeToDefaultSet($code);
        }

        return $this->loadedAttributes[$code];
    }

    /**
     * Preloads all attributes from db
     *
     * @return void
     */
    protected function loadAttributes()
    {
        $productType = Mage::getModel('eav/entity')->setType('catalog_product')->getTypeId();
        $currentAttributes = Mage::getResourceModel('eav/entity_attribute_collection')
            ->addFieldToFilter('entity_type_id', $productType);

        foreach ($currentAttributes as $attribute) {
            /** @var Mage_Eav_Model_Entity_Attribute $attribute */
            if ($attribute->getFrontendInput() != 'select') {
                continue;
            }

            $this->loadedAttributes[$attribute->getAttributeCode()] = $attribute;

            $attributeOptions = Mage::getModel('eav/entity_attribute_source_table');
            $attributeOptions->setAttribute($attribute);
            $options = $attributeOptions->getAllOptions(false);

            foreach ($options as $option) {
                $this->loadedAttributeOptions[$attribute->getAttributeCode()][$option['label']] = true;
            }
        }
    }
}