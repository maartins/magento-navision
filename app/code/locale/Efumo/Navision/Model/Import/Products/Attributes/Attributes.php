<?php

/**
 * Efumo_Navision
 *
 * @category    Efumo
 * @package     Efumo_Navision
 * @author      Martins Saukums
 * @copyright   Copyright (c) 2017 Efumo, Ltd.(https://www.efumo.lv/)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Efumo_Navision_Model_Import_Products_Attributes_Attributes extends Efumo_Navision_Model_Import_Products_Info
{
    const RESET_BEFORE_LOAD = false;
    const NO_FORMAT = '%d';
    const NO_FIELD = 'Item_Attribute_ID';
    const ITEM_NO_FIELD = 'ID';
    const WEB_SERVICE = 'ItemAttribute';

    /**
     * Fetch attribute's label
     *
     * @param $attributeId
     * @return null
     */
    public function getInfo($attributeId)
    {
        if (is_object($attributeId)) {
            $attributeId = $attributeId->Item_Attribute_ID;
        }

        if (!isset($this->data[$attributeId])) {
            return null;
        }

        return trim($this->data[$attributeId]);
    }

    /**
     * Returns an array of product data
     *
     * @param array $collection
     * @return array
     */
    protected function processInfo(array $collection)
    {
        foreach ($collection as $value) {
            $itemNo = $value->{static::ITEM_NO_FIELD};
            $this->data[$itemNo] = $value->Name;
        }
    }
}