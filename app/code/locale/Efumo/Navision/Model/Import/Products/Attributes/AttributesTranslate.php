<?php

/**
 * Efumo_Navision
 *
 * @category    Efumo
 * @package     Efumo_Navision
 * @author      Martins Saukums
 * @copyright   Copyright (c) 2016 Efumo, Ltd.(https://www.efumo.lv/)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Efumo_Navision_Model_Import_Products_Attributes_AttributesTranslate extends Efumo_Navision_Model_Import_Products_Info
{
    const English = 'ENU';
    const Latvian = 'LVI';
    const Estonian = 'ETI';
    const Russian = 'RUS';
    const RESET_BEFORE_LOAD = false;
    const NO_FORMAT = '%d';
    const NO_FIELD = 'Item_Attribute_ID';
    const ITEM_NO_FIELD = 'Attribute_ID';
    const WEB_SERVICE = 'ItemAttributeTranslation';

    /**
     * Fetch options's label
     *
     * @param $valueId
     * @return null
     */
    public function getInfo($valueId)
    {
        if (is_object($valueId)) {
            $valueId = $valueId->Item_Attribute_ID;
        }

        if (!isset($this->data[$valueId])) {
            return null;
        }

        return $this->data[$valueId];
    }

    /**
     * Returns an array of product data
     *
     * @param array $collection
     * @return array
     */
    protected function processInfo(array $collection)
    {
        foreach ($collection as $value) {
            $itemNo = $value->{self::ITEM_NO_FIELD};
            $this->data[$itemNo][$value->Language_Code] = $value->Name;
        }
    }
}