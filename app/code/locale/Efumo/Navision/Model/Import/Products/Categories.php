<?php

/**
 * Efumo_Navision
 *
 * @category    Efumo
 * @package     Efumo_Navision
 * @author      Martins Saukums
 * @copyright   Copyright (c) 2016 Efumo, Ltd.(https://www.efumo.lv/)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Efumo_Navision_Model_Import_Products_Categories extends Efumo_Navision_Model_Import_Abstract
{
    const WEB_SERVICE = 'ItemWebCategories';

    /**
     * @var array
     */
    protected $categories = [];

    public function import()
    {
        // Category - Product relation import
        $this->_getAllCategoryProductRelations();

    }

    protected function _getAllCategoryProductRelations()
    {
        $this->processAllPages(self::WEB_SERVICE, [], function ($collection) {
            if (isset($collection->value)) {
                $this->_setCategories($collection->value);
            }
        });
    }

    protected function _setCategories($data)
    {
        foreach ($data as $relations) {
            $this->categories[$relations->Item_Web_ID][] = $relations->Web_Category_Code;
        }
    }

    /**
     * @return array
     */
    public function getCategories()
    {
        if (!count($this->categories)) {
            $this->import();
        }

        return $this->categories;
    }

}