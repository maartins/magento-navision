<?php

/**
 * Efumo_Navision
 *
 * @category    Efumo
 * @package     Efumo_Navision
 * @author      Martins Saukums
 * @copyright   Copyright (c) 2017 Efumo, Ltd.(https://www.efumo.lv/)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Efumo_Navision_Model_Import_Products_Description extends Efumo_Navision_Model_Import_Products_Info
{
    const WEB_SERVICE = 'ItemTranslation';
    const ATTRIBUTE = 'description';

    /**
     * Returns an array of product data
     *
     * @param array $collection
     * @return array
     */
    protected function processInfo(array $collection)
    {
        foreach ($collection as $value) {
            $itemNo = $value->{static::ITEM_NO_FIELD};
            if (!isset($this->data[$itemNo])) {
                $this->data[$itemNo] = [];
            }

            $this->data[$itemNo][$value->Language_Code] = $value->Description;

        }
    }
}