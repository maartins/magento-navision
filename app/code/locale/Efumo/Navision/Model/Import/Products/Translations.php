<?php

/**
 * Efumo_Navision
 *
 * @category    Efumo
 * @package     Efumo_Navision
 * @author      Martins Saukums
 * @copyright   Copyright (c) 2017 Efumo, Ltd.(https://www.efumo.lv/)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Efumo_Navision_Model_Import_Products_Translations extends Efumo_Navision_Model_Import_Products_Info
{
    const WEB_SERVICE = 'ItemTranslation';

    const ENU = 'English';
    const LVI = 'Latvian';
    const ETI = 'Estonian';
    const RUS = 'Russian';
}