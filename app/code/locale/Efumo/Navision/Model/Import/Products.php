<?php

/**
 * Efumo_Navision
 *
 * @category    Efumo
 * @package     Efumo_Navision
 * @author      Martins Saukums
 * @copyright   Copyright (c) 2016 Efumo, Ltd.(https://www.efumo.lv/)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Efumo_Navision_Model_Import_Products extends Efumo_Navision_Model_Import_Abstract
{
    /**
     * Class responsible for getting product description
     *
     * @var Efumo_Navision_Model_Import_Products_Description
     */
    protected $description;
    /**
     * Class responsible for getting item to category
     *
     * @var Efumo_Navision_Model_Import_Products_Categories
     */
    protected $itemCategories;

    /**
     * @var Efumo_Navision_Model_Import_Products_Stock
     */
    protected $stock;

    /**
     * Class responsible for getting prices
     *
     * @var Efumo_Navision_Model_Import_Products_Prices
     */
    protected $prices;

    /**
     * Class responsible for getting attributes
     *
     * @var Efumo_Navision_Model_Import_Products_Attributes
     */
    protected $attributes;

    /**
     * Class responsible for getting attachments
     *
     * @var Efumo_Navision_Model_Import_Products_Attachments
     */
    protected $attachments;

    /**
     * Class responsible for importing categories
     *
     * @var Efumo_Navision_Model_Import_Categories
     */
    protected $categories;

    /**
     * Language to store mapping
     *
     * @var array
     */
    protected $languageToStore;

    /**
     * Class responsible for getting translations
     *
     * @var Efumo_Navision_Model_Import_Products_Translations
     */
    protected $translations;

    /**
     * @var Efumo_Navision_Model_Import_Products_Configurable
     */
    protected $confugurable;

    /**
     * @var array
     */
    protected $configurableArray = [];

    /**
     * @var array
     */
    protected $productToCateory = [];

    /**
     *  Save used configurable product attributes
     * @var array
     */
    protected $data = [];

    /**
     * @param $web_id
     */
    public function importByWebId($web_id)
    {
        $this->_intiImport($web_id);
    }

    /**
     * Runs import
     *
     * @return void
     */
    public function import()
    {
        $this->_intiImport();
    }

    /**
     * @param $web_id
     */
    protected function _intiImport($web_id = null)
    {
        // run category import first
        // $this->categories = Mage::getSingleton('efumo_navision/import_categories');
//        $this->categories->import();
        $this->itemCategories = Mage::getSingleton('efumo_navision/import_products_categories');
        $this->description = Mage::getSingleton('efumo_navision/import_products_description');
        $this->attachments = Mage::getSingleton('efumo_navision/import_products_attachments');
        $this->attributes = Mage::getSingleton('efumo_navision/import_products_attributes');
        $this->prices = Mage::getSingleton('efumo_navision/import_products_prices');
        // $this->stock = Mage::getSingleton('efumo_navision/import_products_stock');
        $this->confugurable = Mage::getSingleton('efumo_navision/import_products_configurable');
        $this->translations = Mage::getSingleton('efumo_navision/import_products_translations');


        $stores = array('2'/*, 'ee_est'*/);
        foreach ($stores as $store) {
            $this->initStore($store);
            // $this->itemCategories->initStore($this->store);
            $this->translations->initStore();
            $this->productToCateory = $this->itemCategories->getCategories();
            $this->description->initStore();
            $this->attachments->initStore($this->store);
            $this->attributes->initStore();
            // $this->stock->initStore($this->store);
            $this->prices->initStore();
            $this->confugurable->initStore();
            $this->productToCateory = $this->itemCategories->getCategories();
            $this->importProducts($web_id);
        }
    }

    /**
     * Initiates a new store
     *
     * @param null $store
     * @return $this
     */
    public function initStore($store = null)
    {
        parent::initStore($store);

        $this->languageToStore = [];

        foreach (Mage::app()->getStores() as $_eachStoreId => $val) {
            $this->languageToStore[Mage::app()->getStore($_eachStoreId)->getName()] = Mage::app()->getStore($_eachStoreId)->getCode();
        }

        return $this;
    }

    /**
     * Imports products
     *
     * @return void
     */
    protected function importProducts($web_id)
    {
        if (!count($this->configurableArray)) {
            $this->processAllPages('ItemSubstitutions', [], function ($collection) {
                if (count($collection->value)) {
                    $this->configurableArray = array_merge($this->configurableArray, $collection->value);
                }
            });
        }

        $params['$filter'] = ($web_id) ? "Web_ID eq $web_id" : 'Web_Item eq true';
        //['$filter' => 'Web_Item eq true and No eq \'ZI7.0MI0\'']
        //['$filter' => 'Web_Item eq true and No eq \'015.55.639\'']
        // configurable
        //['$filter' => 'No eq \'007.020.20.042\'']
        //

        //['$filter' => 'Web_Item eq true']
        // Two confi attributes ['$filter' => 'No eq \'00.031.108\'']

        $this->processAllPages('ItemCard', ['$filter' => 'Web_Item eq true and No eq \'015.55.639\''], function ($collection) {
            if (isset($collection->value)) {
                $this->loadAdditionalDataForProducts($collection->value);
                $this->processProducts($collection->value);
            }
        });
    }

    /**
     * Processes a batch of products (up to 1000)
     *
     * @param array $products
     * @return void
     */
    protected function processProducts(array $products)
    {
        /** @var AvS_FastSimpleImport_Model_Import $import */
        $import = Mage::getSingleton('fastsimpleimport/import');
        $importData = [];
        $variantIds = array_column(json_decode(json_encode($this->configurableArray), true), 'Substitute_No');

        foreach ($products as $product) {
            if ($product->No && $product->Unit_Price && $product->Description) {
                $importData = array_merge($importData, $this->buildProduct($product, $variantIds));
            }
        }

        Mage::log($importData);
        if ($importData) {
            $import->setAllowRenameFiles(false)
                ->setUseNestedArrays(true)
                ->setPartialIndexing(true)
                ->processProductImport($importData, 'replace');
        }
    }

    /**
     * @param $product
     * @param array $children
     * @return array
     */
    protected function creteConfigurable($product, array $children)
    {
        if (count($children)) {
            $childrenSku = [];

            foreach ($children as $child) {
                array_push($childrenSku, $child->Substitute_No);
            }

            $products = Mage::getResourceModel('catalog/product_collection')
                ->addAttributeToSelect('entity_id')
                ->addAttributeToFilter(
                    'sku', array('in' => $childrenSku));

            if ($products->count()) {
                foreach ($products as $product) {
                    try {
                        Mage::getModel('catalog/product')->load($product->getId())->delete();
                    } catch (Exception $e) {
                        Mage::logException("Could not delete product with ID: " . $product->getId() . "<br />");
                    }
                }
            }
        }

        //Create attribute set

        $data = [
            'visibility' => Mage_Catalog_Model_Product_Visibility::VISIBILITY_IN_SEARCH
        ];

        // Child product create
        $importData = [];
        $description = $this->description->getInfo($product);
        $configurableProduct = [
            'sku' => $product->No . '_configurable',
            '_type' => 'configurable',
            '_attribute_set' => 'Default',
            '_product_websites' => 'base',
            'price' => $product->Unit_Price,
            'name' => $product->Description,
            'short_description' => $product->Search_Description ?: '.',
            'description' => array_key_exists('LV', $description) ? $description["LV"] : '.',
            'weight' => $product->Net_Weight,
            'unit_of_measure' => $product->Sales_Unit_of_Measure ?: __('pieces'),
            'status' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED,
            'visibility' => Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH,
            'tax_class_id' => 2,
            'is_in_stock' => 1,
            'manufacturer' => $product->Manufacturer_Code,
            'web_id' => $product->Web_ID,
            '_super_products_sku' => '',
            '_super_attribute_code' => '',
            '_category' => '',
            '_media_image' => ''
        ];

        $configurableProduct = $this->_addProductToCategory($configurableProduct, $product);

        // Add main product as simple
        $mainSimple = new stdClass;
        $mainSimple->Substitute_No = $product->No;

        $children[] = $mainSimple;
        foreach ($children as $child) {
            $this->processAllPages('ItemCard', ['$filter' => "No eq '$child->Substitute_No'"], function ($collection) use ($data, &$importData) {
                if (isset($collection->value)) {
                    foreach ($collection->value as $product) {
                        $this->loadAdditionalDataForProducts($product);
                        $importData = array_merge($importData, $this->buildProduct($product, [], $data));
                    }
                }
            });
        }


//        if( $images = $this->attachments->getImagesForProduct($product) ) {
//            $configurableProduct['_media_image'] = $images;
//            $configurableProduct['_media_position'] = array_keys($images);
//            ksort($images);
//            $configurableProduct['image'] = reset($images);
//            $configurableProduct['small_image'] = reset($images);
//            $configurableProduct['thumbnail'] = reset($images);
//        }

        $configurableProduct['_super_products_sku'] = array_column($importData, 'sku');
        $configurableProduct['_super_attribute_code'] = $this->data;

        array_push($importData, $configurableProduct);

        //Reset data variable for next configurable product attributes
        $this->data = [];

        return $importData;
    }

    /**
     * Builds product (and it's children)
     *
     * @param $product
     * @return array
     */
    protected function buildProduct($product, $childProducts, $data = null)
    {
        $variants = [];

        if ((in_array($product->No, $childProducts)) && count($childProducts)) {
            return $variants;
        };

        if (count($childProducts) && $product->IsConfigurable) {
            $baseProduct = $this->creteConfigurable($product, array_filter($this->configurableArray, function ($val) use (&$product) {
                return $val->No == $product->No;
            }));


            return $baseProduct;
        }

        // do not process empty products
        if (!$product->Description) {
            return $variants;
        }

        $description = $this->description->getInfo($product);

        // build base product
        $baseProduct = [
            'sku' => $product->No, //Web_ID
            '_type' => 'simple',
            '_product_websites' => 'base',
            '_attribute_set' => (is_array($data) && array_key_exists('set', $data)) ? $data['set'] : 'Default',
            'tax_class_id' => 2, // taxable goods
            'name' => $product->Description ?: $product->No,
            'short_description' => $product->Search_Description ?: '.',
            'description' => array_key_exists('LV', $description) ? $description["LV"] : ($product->Search_Description) ? $product->Search_Description : '.',
            'weight' => $product->Net_Weight,
            'price' => $product->Unit_Price ?: 0,//$this->prices->getInfo($product),
            'qty' => $product->Inventory,
            'is_in_stock' => $product->Inventory > 0 ? 1 : 0,
            'enable_qty_increments' => 1,
            'unit_of_measure' => $product->Sales_Unit_of_Measure ?: __('pieces'),
            'web_id' => $product->Web_ID ?: 0,
            'manufacturer' => $product->Manufacturer_Code,
            'use_config_enable_qty_inc' => 0,
            'qty_increments' => $product->Rounding_Precision,
            'use_config_qty_increments' => 0,
            '_category' => '',
            'status' => '',
            'visibility' => '',
            '_media_image' => '',
            'is_qty_decimal' => intval($product->Rounding_Precision) != $product->Rounding_Precision ? 1 : 0,
        ];

        // add attribute values
        $attributes = $this->attributes->getAttributesForProduct($product);
        $this->data = array_keys($attributes);

        $baseProduct = array_merge($baseProduct, $attributes);

        // TODO: WebDescription
        $baseProduct = $this->_addProductToCategory($baseProduct, $product);

        if ($product->IsConfigurable) {
            $baseProduct['url_key'] = Mage::getModel('catalog/product_url')->formatUrlKey($product->Description . '-1');
        }

//        if( $images = $this->attachments->getImagesForProduct($product) ) {
//            $baseProduct['_media_position'] = array_keys($images);
//            $baseProduct['_media_image'] = $images;
//            ksort($images);
//            $baseProduct['image'] = reset($images);
//            $baseProduct['small_image'] = reset($images);
//            $baseProduct['thumbnail'] = reset($images);
//        }

        $baseProduct['status'] = Mage_Catalog_Model_Product_Status::STATUS_ENABLED;
        $baseProduct['visibility'] = ($data['visibility']) ? $data['visibility'] : Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH;

        $variants[] = $baseProduct;

        foreach ($this->translations->getInfo($product) as $translation) {
            $language = defined("Efumo_Navision_Model_Import_Products_Translations::" . $translation->Language_Code) ? constant("Efumo_Navision_Model_Import_Products_Translations::" . $translation->Language_Code) : null;

            if (!isset($this->languageToStore[$language])) {
                continue;
            }
            $variants[] = [
                '_store' => $this->languageToStore[$language],
                'name' => $translation->Description . $translation->Description_2 ?: $product->Description,
                'description' => ($translation->HTML_Description ?: $product->Search_Description) ?: '.'

            ];
        }

        return $variants;
    }

    /**
     * @param $baseProduct
     * @param $product
     * @return mixed
     */
    protected function _addProductToCategory($baseProduct, $product)
    {
        $categories = $this->productToCateory;
        if ($product->Web_ID && array_key_exists($product->Web_ID, $categories)) {
            $baseProduct['_category'] = [];
            $categoryCollection = Mage::getModel('catalog/category')->getCollection()
                ->addAttributeToSelect('id')
                ->addAttributeToFilter('navision_code', array('in' => $categories[$product->Web_ID]));

            foreach ($categoryCollection as $category) {
                array_push($baseProduct['_category'], $category->getId());
            }

        }

        return $baseProduct;
    }

    /**
     * Loads additional information for products
     *
     * @param $products
     * @return void
     */
    protected function loadAdditionalDataForProducts($products)
    {
        $products = is_array($products) ? $products : func_get_args();
        $this->description->loadInfo($products);
        $this->translations->loadInfo($products);
        // $this->attachments->loadInfo($products);
        $this->confugurable->loadInfo($products);
        $this->attributes->loadInfo($products);
        $this->prices->loadInfo($products);
        //$this->stock->loadInfo($products);
    }
}