<?php

/**
 * Efumo_Navision
 *
 * @category    Efumo
 * @package     Efumo_Navision
 * @author      Martins Saukums
 * @copyright   Copyright (c) 2017 Efumo, Ltd.(https://www.efumo.lv/)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Efumo_Navision_Model_Cron
{
    /**
     * Gets products form queue table and retrieve them
     */
    public function syncProducts()
    {

        try {
            $model = Mage::getModel('efumo_navision/sync')->getCollection()
                ->addFieldToFilter('type', Efumo_Navision_Helper_Data::PRODUCT_TYPE)
                ->addFieldToFilter('retrieve', 1)
                ->getFirstItem();

            $web_id = $model->getItemId();

            Mage::helper('efumo_navision')->deleteProductByWebId($web_id);

            Mage::getModel('efumo_navision/import_products')->importByWebId($web_id);

            $product = Mage::getModel('catalog/product')->loadByAttribute('web_id', $web_id);

            if (!$product) {
                Mage::throwException('Product with web_id' . $web_id . 'not imported');
            } else {
                $model->delete();
            }

        } catch (Exception $e) {
            Mage::logException($e);
        }
    }

    /**
     * Sync special price from queue
     */
    public function syncSpecialPrice()
    {
        try {
            $model = Mage::getModel('efumo_navision/sync')->getCollection()
                ->addFieldToFilter('type', Efumo_Navision_Helper_Data::SPECIAL_PRICE)
                ->addFieldToFilter('retrieve', 1)
                ->getFirstItem();

            Mage::getSingleton('efumo_navision/import_salesprices')->import();

            $model->delete();

        } catch (Exception $e) {
            Mage::logException($e);
        }
    }
}