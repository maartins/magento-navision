<?php

/**
 * Efumo_Navision
 *
 * @category    Efumo
 * @package     Efumo_Navision
 * @author      Martins Saukums
 * @copyright   Copyright (c) 2017 Efumo, Ltd.(https://www.efumo.lv/)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Efumo_Navision_Model_Navision_Soap
{
    const API_URL = "https://zr.navex.lv:19047/AMF2017/WS/AM%20Furnit%C5%ABra%20EUR/Codeunit/WebAPI";

    /**
     * Username
     *
     * @var string
     */
    protected $user;

    /**
     * Password
     *
     * @var string
     */
    protected $password;

    /**
     * Soap API URL
     *
     * @var string
     */
    protected $soap_url;

    /**
     * Soap Client
     *
     * @var SoapClient
     */
    protected $client;

    /**
     * Initialize API info
     *
     * @param int|null $storeId
     */
    public function __construct($storeId = null)
    {
        $storeId = $storeId ?: null;

        $this->user = Mage::getStoreConfig('navision/api/user', $storeId);
        $this->password = Mage::getStoreConfig('navision/api/password', $storeId);
        $this->soap_url = rtrim(self::API_URL, ' /');

        $this->client = new SoapClient($this->soap_url, array(
                'login' => $this->user,
                'password' => $this->password,
            )
        );
    }

    /**
     * Performs request and returns response as JSON object
     *
     * @param       $method
     * @param array $params
     * @return mixed
     */
    public function request($method, $params = [])
    {
        return $this->client->__soapCall($method, [$params]);
    }

    public function getLastRequest()
    {
        return $this->client->__getLastRequest();
    }
}