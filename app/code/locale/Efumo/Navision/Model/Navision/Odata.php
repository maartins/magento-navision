<?php

/**
 * Efumo_Navision
 *
 * @category    Efumo
 * @package     Efumo_Navision
 * @author      Martins Saukums
 * @copyright   Copyright (c) 2017 Efumo, Ltd.(https://www.efumo.lv/)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Efumo_Navision_Model_Navision_Odata
{
    const BATCH_BOUNDARY = 'NAVISION-BOUNDARY';

    /**
     * Username
     *
     * @var string
     */
    protected $user;

    /**
     * Password
     *
     * @var string
     */
    protected $password;

    /**
     * OData URL
     *
     * @var string
     */
    protected $odata_url;

    /**
     * Initialize API info
     *
     * @param int|null $storeId
     */
    public function __construct($storeId = null)
    {
        $storeId = $storeId ?: null;
        $this->user = Mage::getStoreConfig('navision/api/user', $storeId);
        $this->password = Mage::getStoreConfig('navision/api/password', $storeId);
        $this->odata_url = rtrim(Mage::getStoreConfig('navision/api/url', $storeId) . Mage::getStoreConfig('navision/api/company', $storeId), ' /');
    }

    /**
     * Performs a single batch request - wrapper for many calls to OData
     *
     * @param       $webService
     * @param array $batches
     * @return mixed
     */
    public function batch($webService, array $batches)
    {
        $values = [];

        $body = $this->buildBatchBody($webService, $batches);

        $response = $this->request(
            'POST', $this->odata_url . '$batch', $body,
            ['Content-Type: multipart/mixed; boundary=' . self::BATCH_BOUNDARY,], false
        );

        $batchResponses = $this->getBatchResponses($response);
        foreach ($batchResponses as $response) {
            $content = explode("\n\n", $response, 3);
            if (isset($content[2])) {
                $collection = json_decode($content[2]);

                // automatically assign all collections
                if ($collection && isset($collection->value)) {
                    $values = array_merge($values, $collection->value);
                }
            }
        }

        return $values;
    }

    /**
     * Helper method for creating models
     *
     * @param       $webService
     * @param array $data
     * @return mixed
     */
    public function create($webService, array $data)
    {
        return $this->request('POST', $webService, json_encode($data), ['Content-Type: application/json;']);
    }

    /**
     * Find web service element by primary key
     *
     * @param $webService
     * @param $primaryKey
     * @return mixed
     */
    public function find($webService, $primaryKey)
    {
        $requestUrl = $this->getModelUrl($webService, $primaryKey);
        $response = $this->request('GET', $requestUrl);

        if (!$response || isset($response->{'odata.error'})) {
            return false;
        }

        return $response;
    }

    /**
     * Delete a model by its primary key
     *
     * @param      $webService
     * @param      $primaryKey
     * @param null $eTag
     * @return bool
     */
    public function delete($webService, $primaryKey, $eTag = null)
    {
        if (!$eTag) {
            $model = $this->find($webService, $primaryKey);

            // if model not found, return false because we have no etag
            if (!$model) {
                return false;
            }

            $eTag = $model->ETag;
        }

        $requestUrl = $this->getModelUrl($webService, $primaryKey);
        $response = $this->request('DELETE', $requestUrl, [], ['If-Match: W/"\'' . urlencode($eTag) . '\'"']);

        if ($response) {
            return false;
        }

        return true;
    }

    /**
     * Performs request and returns response as JSON object
     *
     * @param       $method
     * @param       $url
     * @param array $params
     * @param array $headers
     * @param bool $asObject
     * @return mixed
     */
    public function request($method, $url, $params = [], $headers = [], $asObject = true)
    {
        $method = strtoupper($method);
        Mage::log($this->modifyUrl($url, $method, $params));
        $curl = curl_init($this->modifyUrl($url, $method, $params));

        // prepare request
        //curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_NTLM);
        curl_setopt($curl, CURLOPT_UNRESTRICTED_AUTH, true);
        curl_setopt($curl, CURLOPT_USERPWD, $this->user . ":" . $this->password);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);

        // add headers
        if ($headers) {
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        }
        if ($method != 'GET') {
            curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        }

        curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
        // execute
        $curl_response = curl_exec($curl);
        curl_close($curl);

        return $asObject ? json_decode($curl_response) : $curl_response;
    }

    /**
     * Adds required pieces to url
     *
     * @param       $originalUrl
     * @param       $method
     * @param array $params
     * @return string
     */
    protected function modifyUrl($originalUrl, $method, $params = [])
    {
        $url = parse_url($originalUrl);
        $newUrl = $this->odata_url;

        if (isset($url['host'])) {
            $newUrl = sprintf('%s://%s:%d/%s', $url['scheme'], $url['host'], $url['port'], ltrim($url['path'], '/'));
        } else {
            $newUrl .= sprintf('/%s', ltrim($url['path'], '/'));
        }

        // parse queries from url
        $query = [];
        if (isset($url['query'])) {
            parse_str($url['query'], $query);
        }

        // add queries from params
        if ($method == 'GET') {
            $query = array_merge($query, $params);
        }

        // set output format as json
        //$query['$format'] = 'application/json';
        if ($query) {
            $newUrl .= '?' . http_build_query($query);
        }

        return $newUrl;
    }

    /**
     * Fetch formatted model url
     *
     * @param $webService
     * @param $primaryKey
     * @return string
     */
    public function getModelUrl($webService, $primaryKey)
    {
        $params = [];
        $primaryKey = (array)$primaryKey;

        foreach ($primaryKey as $field => $value) {
            if (!is_numeric($field)) {
                $value = "{$field}='{$value}'";
            }

            $params[] = $value;
        }

        $params = implode(',', $params);
        return "{$webService}({$params})";
    }

    /**
     * @param       $webService
     * @param array $batches
     * @return array|string
     */
    protected function buildBatchBody($webService, array $batches)
    {
        $body = [];
        foreach ($batches as $batch) {
            $body[] = implode(PHP_EOL, [
                '--' . self::BATCH_BOUNDARY,
                'Content-Type: application/http',
                'Content-Transfer-Encoding: binary',
                PHP_EOL,
                'GET ' . iconv('CP1257', 'UTF-8', $this->modifyUrl($webService, 'GET', $batch)) . ' HTTP/1.1',
                PHP_EOL,
            ]);
        }
        $body[] = '--' . self::BATCH_BOUNDARY . '--';

        return implode(PHP_EOL, $body);
    }

    /**
     * @param $response
     * @return array
     */
    protected function getBatchResponses($response)
    {
        $lines = explode("\n", $response);
        $boundary = trim($lines[0]);
        $lines = implode("\n", array_map('trim', array_slice($lines, 1)));

        $batchResponses = explode($boundary, str_replace($boundary . '--', '', $lines));
        return $batchResponses;
    }
}