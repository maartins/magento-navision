<?php

/**
 * Efumo_Navision
 *
 * @category    Efumo
 * @package     Efumo_Navision
 * @author      Martins Saukums
 * @copyright   Copyright (c) 2017 Efumo, Ltd.(https://www.efumo.lv/)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
abstract class Efumo_Navision_Model_Export_Abstract
{
    /**
     * API instance
     *
     * @var Efumo_Navision_Model_Navision_Odata
     */
    protected $api;

    /**
     * Initiate API instance and FastSimpleImport
     */
    public function __construct()
    {
        $this->api = Mage::getSingleton('efumo_navision/navision_odata');
    }

    /**
     * Export data
     *
     * @param Varien_Event_Observer $model
     */
    abstract public function export(Varien_Event_Observer $model);
}