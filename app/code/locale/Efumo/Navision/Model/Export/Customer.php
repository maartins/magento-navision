<?php

/**
 * Efumo_Navision
 *
 * @category    Efumo
 * @package     Efumo_Navision
 * @author      Martins Saukums
 * @copyright   Copyright (c) 2017 Efumo, Ltd.(https://www.efumo.lv/)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Efumo_Navision_Model_Export_Customer extends Efumo_Navision_Model_Export_Abstract
{
    /**
     * Sync new customer data to navision
     *
     * @param Varien_Event_Observer $observer
     */
    public function export(Varien_Event_Observer $observer)
    {
        /** @var Mage_Customer_Model_Customer $customer */
        $customer = $observer->getCustomer();

        if ($contactNumber = Mage::app()->getRequest()->getParam('navision_contact_number')) {
            $customer->setNavisionContactNumber($contactNumber);

            // set client number
            if ($clientNumber = Mage::getSingleton('efumo_navision/import_customers')->getClientNumber($customer)) {
                $customer->setNavisionClientNumber($clientNumber);
            }
        }
    }
}