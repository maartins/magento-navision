<?php
/**
 * Efumo_Navision
 *
 * @category    Efumo
 * @package     Efumo_Navision
 * @author      Martins Saukums
 * @copyright   Copyright (c) 2016 Efumo, Ltd.(https://www.efumo.lv/)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Efumo_Navision_Model_Export_Order extends Efumo_Navision_Model_Export_Abstract
{
    /**
     * Sync new order data to navision
     *
     * @param Varien_Event_Observer $observer
     */
    public function export(Varien_Event_Observer $observer)
    {
        /** @var Mage_Sales_Model_Order $order */
        $order = $observer->getOrder();
        if (!$order->getCustomerId()) {
            return;
        }

        $customer = Mage::getModel('customer/customer')->load($order->getCustomerId());
        $billingAddress = $order->getBillingAddress();
        $shippingAddress = $order->getShippingAddress();

        $response = $this->createOrder($order, $customer, $billingAddress, $shippingAddress);
        foreach ($order->getAllVisibleItems() as $item) {
            $this->createItem($response, $item);
        }

        Mage::dispatchEvent('efumo_navision_order_sent',['custumer' => $customer]);

    }

    /**
     * Creates order
     *
     * @param $order
     * @param $customer
     * @param $billingAddress
     * @param $shippingAddress
     * @return array
     */
    protected function createOrder($order, $customer, $billingAddress, $shippingAddress)
    {
        $data = [
            'Document_Type'        => 'Order',
            'External_Document_No' => $order->getIncrementId(),
            'Sell_to_Customer_No'   => 'K130140',
            //'Sell_to_Contact_No'   => $customer->getNavisionContactNumber(),
            'Sell_to_Customer_Name' => $customer->getCompany() ?: $order->getCustomerName(),
            'Sell_to_Address'      => $billingAddress->getStreet1(),
            'Sell_to_Address_2'    => $billingAddress->getStreet2(),
            'Sell_to_Post_Code'    => $billingAddress->getPostcode(),
            'Sell_to_City'         => $billingAddress->getCity(),
            'Bill_to_Customer_No'  => $customer->getNavisionClientNumber(),
            'Bill_to_Contact_No'   => $customer->getNavisionContactNumber(),
            'Bill_to_Name'         => $customer->getCompany() ?: $order->getCustomerName(),
            'Bill_to_Address'      => $billingAddress->getStreet1(),
            'Bill_to_Address_2'    => $billingAddress->getStreet2(),
            'Bill_to_Post_Code'    => $billingAddress->getPostcode(),
            'Bill_to_City'         => $billingAddress->getCity(),
            'Ship_to_Name'         => $customer->getCompany() ?: $order->getCustomerName(),
            'Ship_to_Address'      => $shippingAddress->getStreet1(),
            'Ship_to_Address_2'    => $shippingAddress->getStreet2(),
            'Ship_to_Post_Code'    => $shippingAddress->getPostcode(),
            'Ship_to_City'         => $shippingAddress->getCity(),
            'Salesperson_Code'     => '1',
        ];

        return $this->api->create('SalesOrder', $data);
    }

    /**
     * Creates order item
     *
     * @param $response
     * @param $item
     * @return void
     */
    protected function createItem($response, $item)
    {
        /** @var Mage_Sales_Model_Order_Item $item */
        $data = [
            'Document_Type' => 'Order',
            'Document_No'   => $response->No,
            'Type'          => 'Item',
            'No'            => $item->getSku(),
            'Quantity'      => $item->getQtyOrdered()

//            TODO: add partial order count!!!
        ];

        $response = $this->api->create('SalesOrderSalesLines', $data);
        $this->createItemComment($response, $item);
    }

    /**
     * Creates order item
     *
     * @param $response
     * @param $item
     * @return void
     */
    protected function createItemComment($response, $item)
    {
        /** @var Mage_Sales_Model_Order_Item $item */

        $comment = null;
        $options = $item->getProductOptionByCode('additional_options');
        if (count($options)) {
            foreach ($options as $option) {
                if (isset($option['code']) && $option['code'] == 'comment') {
                    $comment = $option['value'];
                }
            }
        }

        if (!$comment) {
            return;
        }

        $data = [
            'Document_Type' => 'Order',
            'No'            => $response->Document_No,
            'Line_No'       => $response->Line_No,
            'Comment'       => $comment,
        ];

        $this->api->create('SalesOrderSalesLineComment', $data);
    }

}