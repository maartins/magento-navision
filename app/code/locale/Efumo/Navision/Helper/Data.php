<?php

/**
 * Efumo_Navision
 *
 * @category    Efumo
 * @package     Efumo_Navision
 * @author      Martins Saukums
 * @copyright   Copyright (c) 2017 Efumo, Ltd.(https://www.efumo.lv/)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Efumo_Navision_Helper_Data extends Mage_Core_Helper_Abstract
{
    const PRODUCT_TYPE = 'Product';
    const SPECIAL_PRICE = 'Special_Price';

    /**
     * Fetches information from navision
     *
     * @param        $registrationNo
     * @param string $email
     * @param string $firstName
     * @param string $lastName
     * @return mixed
     */
    public function fetchCompanyInfo($registrationNo, $email = '', $firstName = '', $lastName = '')
    {
        $response = '';
        try {
            $company = Mage::getSingleton('efumo_navision/navision_soap')
                ->request('Register', compact('registrationNo', 'email', 'firstName', 'lastName', 'response'))
                ->response;

            $request = Mage::app()->getRequest();

            if (!$company->Country) {
                $company->Country = $request->getParam('country_id');
            }

            if (!$company->City) {
                $company->City = $request->getParam('city');
            }

            if (!$company->Post_Code) {
                $company->Post_Code = $request->getParam('postcode');
            }

            $postedStreet = $request->getPost('street', []);
            if (!$company->Address && isset($postedStreet[0])) {
                $company->Address = $postedStreet[0];
            }

            if (!$company->Address2 && isset($postedStreet[1])) {
                $company->Address2 = $postedStreet[1];
            }

            if (!$company->Country) {
                $company->Country = Mage::getStoreConfig('general/country/default');
            }

            $company->RegistrationNo = $registrationNo;
        } catch (\Exception $e) {
            return new Varien_Object();
        }

        return $company;
    }

    public function deleteProductByWebId($web_id)
    {
        $_product = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('web_id', array('eq' => $web_id))
            ->getFirstItem();

        if ($_product->getTypeId() == 'configurable') {
            Mage::getModel('eav/entity_attribute_set')->load($_product->getAttributeSetId())->delete();
        }
        $_product->delete();

    }


    /**
     * @param $web_id
     */
    public function setProductInQueue($web_id) {
        Mage::getModel('efumo_navision/sync')->addData(
            [
                'type' => self::PRODUCT_TYPE,
                'item_id' => $web_id,
                'retrieve' => 1
            ]
        )->save();
    }

    /**
     * Update special price sync
     */
    public function setSpecialPriceQueue() {
        $collection = Mage::getModel('efumo_navision/sync')->getCollection()
            ->addFieldToFilter('type',self::SPECIAL_PRICE);

        foreach ($collection as $data) {
            $data->delete();
        }

        Mage::getModel('efumo_navision/sync')->addData(
            [
                'type' => self::SPECIAL_PRICE,
                'retrieve' => 1
            ]
        )->save();
    }
}